package org.compi2;

import java.util.ArrayList;
import java.util.HashMap;

public class API {

	public static HashMap<String, Simbolo> funciones = new HashMap<>();

	public static void inicializar() {
		// Linea(int x1, int y1, int x2, int y2, int r, int g, int b)
		ArrayList<Simbolo> linea = new ArrayList<>();
		linea.add(crearParam("x1", 1, Simbolo.ENTERO, Simbolo.VALOR));
		linea.add(crearParam("y1", 2, Simbolo.ENTERO, Simbolo.VALOR));
		linea.add(crearParam("x2", 3, Simbolo.ENTERO, Simbolo.VALOR));
		linea.add(crearParam("y2", 4, Simbolo.ENTERO, Simbolo.VALOR));
		linea.add(crearParam("r", 5, Simbolo.ENTERO, Simbolo.VALOR));
		linea.add(crearParam("g", 6, Simbolo.ENTERO, Simbolo.VALOR));
		linea.add(crearParam("b", 7, Simbolo.ENTERO, Simbolo.VALOR));

		// Texto(String cadena, int x, int y, int r, int g, int b)
		ArrayList<Simbolo> texto = new ArrayList<>();
		texto.add(crearParam("cadena", 1, Simbolo.CADENA, Simbolo.VALOR));
		texto.add(crearParam("x", 2, Simbolo.ENTERO, Simbolo.VALOR));
		texto.add(crearParam("y", 3, Simbolo.ENTERO, Simbolo.VALOR));
		texto.add(crearParam("r", 4, Simbolo.ENTERO, Simbolo.VALOR));
		texto.add(crearParam("g", 5, Simbolo.ENTERO, Simbolo.VALOR));
		texto.add(crearParam("b", 6, Simbolo.ENTERO, Simbolo.VALOR));

		// Arco(int x, int y, int alto, int ancho, int angInit, int grados, int
		// r, int g, int b, bool fill)
		ArrayList<Simbolo> arco = new ArrayList<>();
		arco.add(crearParam("x", 1, Simbolo.ENTERO, Simbolo.VALOR));
		arco.add(crearParam("y", 2, Simbolo.ENTERO, Simbolo.VALOR));
		arco.add(crearParam("alto", 3, Simbolo.ENTERO, Simbolo.VALOR));
		arco.add(crearParam("ancho", 4, Simbolo.ENTERO, Simbolo.VALOR));
		arco.add(crearParam("angInit", 5, Simbolo.ENTERO, Simbolo.VALOR));
		arco.add(crearParam("grados", 6, Simbolo.ENTERO, Simbolo.VALOR));
		arco.add(crearParam("r", 7, Simbolo.ENTERO, Simbolo.VALOR));
		arco.add(crearParam("g", 8, Simbolo.ENTERO, Simbolo.VALOR));
		arco.add(crearParam("b", 9, Simbolo.ENTERO, Simbolo.VALOR));
		arco.add(crearParam("fill", 10, Simbolo.LOGICO, Simbolo.VALOR));

		// Rectangulo(int x, int y, int alto, int ancho, int r, int g, int b,
		// bool fill)
		ArrayList<Simbolo> rectangulo = new ArrayList<>();
		rectangulo.add(crearParam("x", 1, Simbolo.ENTERO, Simbolo.VALOR));
		rectangulo.add(crearParam("y", 2, Simbolo.ENTERO, Simbolo.VALOR));
		rectangulo.add(crearParam("alto", 3, Simbolo.ENTERO, Simbolo.VALOR));
		rectangulo.add(crearParam("ancho", 4, Simbolo.ENTERO, Simbolo.VALOR));
		rectangulo.add(crearParam("r", 5, Simbolo.ENTERO, Simbolo.VALOR));
		rectangulo.add(crearParam("g", 6, Simbolo.ENTERO, Simbolo.VALOR));
		rectangulo.add(crearParam("b", 7, Simbolo.ENTERO, Simbolo.VALOR));
		rectangulo.add(crearParam("fill", 8, Simbolo.LOGICO, Simbolo.VALOR));

		// Ovalo(int x, int y, int ancho, int alto, int r, int g, int b, bool
		// fill)
		ArrayList<Simbolo> ovalo = new ArrayList<>();
		ovalo.add(crearParam("x", 1, Simbolo.ENTERO, Simbolo.VALOR));
		ovalo.add(crearParam("y", 2, Simbolo.ENTERO, Simbolo.VALOR));
		ovalo.add(crearParam("ancho", 3, Simbolo.ENTERO, Simbolo.VALOR));
		ovalo.add(crearParam("alto", 4, Simbolo.ENTERO, Simbolo.VALOR));
		ovalo.add(crearParam("r", 5, Simbolo.ENTERO, Simbolo.VALOR));
		ovalo.add(crearParam("g", 6, Simbolo.ENTERO, Simbolo.VALOR));
		ovalo.add(crearParam("b", 7, Simbolo.ENTERO, Simbolo.VALOR));
		ovalo.add(crearParam("fill", 8, Simbolo.LOGICO, Simbolo.VALOR));

		// Poligono(Array(int) puntos, Array, int r, int g, int b, bool fill)
		ArrayList<Simbolo> poligono = new ArrayList<>();
		poligono.add(crearParam("puntosX", 1, Simbolo.ENTERO, Simbolo.ARRAY));
		poligono.add(crearParam("puntosY", 2, Simbolo.ENTERO, Simbolo.ARRAY));
		poligono.add(crearParam("r", 3, Simbolo.ENTERO, Simbolo.VALOR));
		poligono.add(crearParam("g", 4, Simbolo.ENTERO, Simbolo.VALOR));
		poligono.add(crearParam("b", 5, Simbolo.ENTERO, Simbolo.VALOR));
		poligono.add(crearParam("fill", 6, Simbolo.LOGICO, Simbolo.VALOR));
		poligono.add(crearParam("dims", 7, Simbolo.ENTERO, Simbolo.VALOR));

		// Lienzo(int ancho, int alto, int r, int g, int b)
		ArrayList<Simbolo> lienzo = new ArrayList<>();
		lienzo.add(crearParam("ancho", 1, Simbolo.ENTERO, Simbolo.VALOR));
		lienzo.add(crearParam("alto", 2, Simbolo.ENTERO, Simbolo.VALOR));
		lienzo.add(crearParam("r", 3, Simbolo.ENTERO, Simbolo.VALOR));
		lienzo.add(crearParam("g", 4, Simbolo.ENTERO, Simbolo.VALOR));
		lienzo.add(crearParam("b", 5, Simbolo.ENTERO, Simbolo.VALOR));

		funciones.put("met_linea_int_int_int_int_int_int_int",
				crearMetodo("linea", 0, Simbolo.VACIO, Simbolo.METODO, linea));
		funciones.put("met_texto_string_int_int_int_int_int",
				crearMetodo("texto", 0, Simbolo.VACIO, Simbolo.METODO, texto));
		funciones.put("met_arco_int_int_int_int_int_int_int_int_int_boolean",
				crearMetodo("arco", 0, Simbolo.VACIO, Simbolo.METODO, arco));
		funciones.put(
				"met_rectangulo_int_int_int_int_int_int_int_boolean",
				crearMetodo("rectangulo", 0, Simbolo.VACIO, Simbolo.METODO,
						rectangulo));
		funciones.put("met_ovalo_int_int_int_int_int_int_int_boolean",
				crearMetodo("ovalo", 0, Simbolo.VACIO, Simbolo.METODO, ovalo));
		funciones.put(
				"met_poligono_int_int_int_int_int_boolean",
				crearMetodo("poligono", 0, Simbolo.VACIO, Simbolo.METODO,
						poligono));
		funciones
				.put("met_lienzo_int_int_int_int_int",
						crearMetodo("lienzo", 0, Simbolo.VACIO, Simbolo.METODO,
								lienzo));
	}

	private static Simbolo crearParam(String id, int posicion, String tipo,
			String tipo2) {
		Simbolo s = new Simbolo();
		s.setId(id);
		s.setTipo(tipo);
		s.setTipo2(tipo2);
		s.setTamano(1);
		s.setLugar(Simbolo.STACK);
		s.setPos(posicion);
		return s;
	}

	private static Simbolo crearMetodo(String id, int posicion, String tipo,
			String tipo2, ArrayList<Simbolo> vars) {
		Simbolo s = new Simbolo();
		s.setId(id);
		s.setTipo(tipo);
		s.setTipo2(tipo2);
		s.setTamano(1);
		s.setLugar(null);
		s.setPos(posicion);
		s.putExtra(vars);
		s.setAmbiente("");
		s.setAcceso(Simbolo.PUBLICO);
		return s;
	}

	public static Simbolo buscarMetodo(String key_id, int linea, int columna) {
		outer: for (String key : funciones.keySet()) {
			if (key_id.equals(key))
				return funciones.get(key);

			String keyPreffix = key_id.replaceAll("met_([A-Za-z0-9]+)_.+",
					"met_$1_");
			if (key.startsWith(keyPreffix)) {
				String callParams[] = key_id.substring(keyPreffix.length(),
						key_id.length()).split("_");
				String metParams[] = key.substring(keyPreffix.length(),
						key.length()).split("_");

				if (callParams.length == metParams.length) {
					for (int i = 0; i < metParams.length; i++) {
						if (!Generador.castImplicito(callParams[i],
								metParams[i]))
							continue outer;
					}

					return funciones.get(key);
				} else
					continue outer;
			}
		}
		return null;
	}
}
