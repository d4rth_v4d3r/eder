package org.compi2;

public class StructTerminal {

	private String etqV, etqF, etqS, etqI, etqR, var;

	public StringBuffer cad;

	private String varT;
	private Simbolo s;

	public StructTerminal(String etqV, String etqF, String etqS, String etqI,
			String etqR, String var) {
		// TODO Auto-generated constructor stub
		setEtqF(etqF);
		setEtqI(etqI);
		setEtqV(etqV);
		setEtqS(etqS);
		setEtqR(etqR);
		setVar(var);
		cad = new StringBuffer();
		varT = null;
	}

	public void setS(Simbolo s) {
		this.s = s;
	}

	public Simbolo getS() {
		return this.s;
	}

	public StructTerminal() {
		cad = new StringBuffer();
		varT = null;
	}

	public String getEtqV() {
		return etqV;
	}

	public void setEtqV(String etqV) {
		this.etqV = etqV;
	}

	public String getEtqF() {
		return etqF;
	}

	public void setEtqF(String etqF) {
		this.etqF = etqF;
	}

	public String getEtqS() {
		return etqS;
	}

	public void setEtqS(String etqS) {
		this.etqS = etqS;
	}

	public String getEtqI() {
		return etqI;
	}

	public void setEtqI(String etqI) {
		this.etqI = etqI;
	}

	public String getVar() {
		return var;
	}

	public void setVar(String var) {
		this.var = var;
	}

	public String getEtqR() {
		return etqR;
	}

	public void setEtqR(String etqR) {
		this.etqR = etqR;
	}

	public StringBuffer getCad() {
		return cad;
	}

	public void setCad(StringBuffer cad) {
		this.cad = cad;
	}

	public String getVarT() {
		return varT;
	}

	public void setVarT(String varT) {
		this.varT = varT;
	}

}
