/**
 * 
 */
package org.compi2;

import java.util.Hashtable;

public class MisClases extends Hashtable<String, MisSimbolos> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8172811991242831513L;

	public int crearClase(String clase, MisSimbolos ts) {
		MisSimbolos c = this.get(clase);
		if (c != null)
			return -1;

		this.put(clase, ts);
		return 0;
	}

	public int crearClaseHija(String claseHija, String clasePadre,
			MisSimbolos ts) {
		MisSimbolos padre = this.get(clasePadre);
		if (padre == null)
			return -2;

		ts.setPadre(padre);
		ts.setTamano(padre.getTamano());

		return crearClase(claseHija, ts);
	}

	@Override
	public synchronized String toString() {
		// TODO Auto-generated method stub
		StringBuffer sb = new StringBuffer();

		for (MisSimbolos ts : values()) {
			sb.append(Simbolo.newline);
			sb.append(MisSimbolos.plain_line);
			sb.append(Simbolo.newline);
			sb.append(String.format(
					"|%-53s%-148s%-53s|",
					" ",
					ts.getClase()
							+ (ts.getPadre() == null ? "" : " - "
									+ ts.getPadre().getClase()) + " | "
							+ ts.getTamano(), " "));
			sb.append(Simbolo.newline);
			sb.append(ts.toString());
			sb.append(Simbolo.newline);
		}

		return sb.toString();
	}

	public StringBuffer toHTML() {
		StringBuffer html = new StringBuffer();

		for (MisSimbolos ts : values()) {
			html.append("<H1> Clase: ");
			html.append(ts.getClase());
			html.append("</H1>");
			html.append(ts.toHTML());
		}

		return html;
	}

}
