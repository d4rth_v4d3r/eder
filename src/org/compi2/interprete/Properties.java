package org.compi2.interprete;

public class Properties {
	public static double __epsilon__ = 0.00000000000000001;
	public static int __mem_size__ = 100 * 1024;
	public static boolean __pusht__ = true;
	public static String __main__ = "main()";
	public static int __temps__ = -1;
	private static final String file_separator = System
			.getProperty("file.separator");
	public static String __path__ = System.getProperty("user.home")
			+ file_separator + "Desktop" + file_separator + "MySQL.db";
}
