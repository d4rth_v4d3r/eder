package org.compi2.interprete;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Hashtable;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JTextArea;

import org.apache.commons.io.FileUtils;
import org.compi2.Generador;

public class Interprete {
	private static final String expr1 = "(\\w+)\\s*=\\s*(\\w+|((\\d+)(\\.\\d+)?))\\s*(\\+|-|\\*|/|%)\\s*(\\w+|((\\d+)(\\.\\d+)?))\\s*;\\s*(?://.*)?";
	private static final String expr2 = "(\\w+)\\s*=\\s*-\\s*(\\w+|((\\d+)(\\.\\d+)?));\\s*(?://.*)?";
	private static final String expr3 = "(\\w+)\\s*=\\s*(\\w+|((\\d+)(\\.\\d+)?));\\s*(?://.*)?";
	private static final String expr4 = "(\\w+)\\s*=\\s*((stack|heap)\\s*\\[\\s*(\\w+|((\\d+)(\\.\\d+)?))\\s*\\])\\s*;\\s*(?://.*)?";
	private static final String expr5 = "((stack|heap)\\s*\\[\\s*(\\w+|((\\d+)(\\.\\d+)?))\\s*\\])\\s*=\\s*(\\w+|((\\d+)(\\.\\d+)?))\\s*;\\s*(?://.*)?";
	private static final String expr6 = "if\\s*(\\w+|((\\d+)(\\.\\d+)?))\\s*(==|!=|<|>|<=|>=)\\s*(\\w+|((\\d+)(\\.\\d+)?))\\s*goto\\s*(\\w+)\\s*;\\s*(?://.*)?";
	private static final String expr7 = "(print|fprint|printf|fprintf)\\s*\\(\\s*\"(%c|%f|%d)\"\\s*,\\s*(\\w+|((\\d+)(\\.\\d+)?))\\s*\\)\\s*;\\s*(?://.*)?";
	private static final String expr8 = "goto\\s*(\\w+);\\s*(?://.*)?";
	private static final String expr9 = "(?://.*)?";
	private static final String expr10 = "method\\s+(\\w+)\\(\\) \\{(?://.*)?";
	private static final String expr11 = "(L\\d+)\\s*:\\s*(?://.*)?";
	private static final String expr12 = "\\}(?://.*)?";
	private static final String expr13 = "call\\s+(\\w*(_\\w*))\\s*\\(\\s*\\)\\s*;\\s*(?://.*)?";
	private static final String tmp = "t(\\d+)", ap = "p", ah = "h",
			num = "(\\d+)(\\.\\d+)?",
			array = "(stack|heap)\\s*\\[\\s*(\\w+|((\\d+)(\\.\\d+)?))\\s*\\]";
	private static Matcher m;

	private Hashtable<String, Integer> metodos, etiquetas;
	private Double[] temps, stack, heap;
	private Double p, h;
	private Pattern[] patrones;
	private Pattern p_tmp, p_p, p_h, p_num, p_array;
	private long fsize;
	private String file;
	private int ptr;
	private final String nl = "\n";//System.getProperty("line.separator");
	private Stack<Double[]> pila;
	private StringBuffer buff;
	public boolean stop;
	public static JTextArea consola;
	public final BufferedImage image = (new BufferedImage(500, 500,
			BufferedImage.TYPE_INT_ARGB));
	public StringBuilder errores = new StringBuilder();
	public int height = 600, width = 600;

	public Interprete(File f) {
		this.fsize = readFile(f);
		this.prepare();
	}

	public Interprete(String filename) {
		this.fsize = readFile(filename);
		this.prepare();
	}

	private void prepare() {
		this.metodos = new Hashtable<String, Integer>();
		this.etiquetas = new Hashtable<String, Integer>();
		this.stop = false;
		this.patrones = new Pattern[13];
		this.patrones[0] = Pattern.compile(expr1);
		this.patrones[1] = Pattern.compile(expr2);
		this.patrones[2] = Pattern.compile(expr3);
		this.patrones[3] = Pattern.compile(expr4);
		this.patrones[4] = Pattern.compile(expr5);
		this.patrones[5] = Pattern.compile(expr6);
		this.patrones[6] = Pattern.compile(expr7);
		this.patrones[7] = Pattern.compile(expr8);
		this.patrones[8] = Pattern.compile(expr9);
		this.patrones[9] = Pattern.compile(expr10);
		this.patrones[10] = Pattern.compile(expr11);
		this.patrones[11] = Pattern.compile(expr12);
		this.patrones[12] = Pattern.compile(expr13);
		this.p_tmp = Pattern.compile(tmp);
		this.p_p = Pattern.compile(ap);
		this.p_h = Pattern.compile(ah);
		this.p_num = Pattern.compile(num);
		this.p_array = Pattern.compile(array);
		this.stack = new Double[Properties.__mem_size__];
		this.heap = new Double[Properties.__mem_size__];
		this.temps = new Double[t_size() + 1];
		this.ptr = 0;
		this.p = new Double(0);
		this.h = new Double(0);
		if (Properties.__pusht__)
			pila = new Stack<Double[]>();
		this.buff = new StringBuffer();
		initTemps();
		consola.setText("");
		System.gc();
	}

	public void run() {
		execute(Properties.__main__);
		writeFile();
	}

	private void pusha() {
		Double[] tmps = new Double[this.temps.length];
		for (int i = 0; i < tmps.length; i++)
			tmps[i] = this.temps[i];

		this.pila.push(tmps);
	}

	private void popa() {
		Double[] tmps = this.pila.pop();
		for (int i = 0; i < tmps.length; i++)
			this.temps[i] = tmps[i];
	}

	private void initTemps() {
		for (int i = 0; i < temps.length; i++)
			temps[i] = new Double(0);
		for (int i = 0; i < stack.length; i++)
			stack[i] = new Double(0);
		for (int i = 0; i < heap.length; i++)
			heap[i] = new Double(0);
	}

	private long readFile(String filename) {
		File f = new File(filename);
		try {
			file = FileUtils.readFileToString(f, Charset.defaultCharset())
					.trim();
			System.gc();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return file.length();
	}

	private long readFile(File f) {
		try {
			file = FileUtils.readFileToString(f, Charset.defaultCharset())
					.trim();
			System.gc();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return file.length();
	}

	private int t_size() {
		String temps = file.substring((int) fsize - 100, (int) fsize);
		int last_t = temps.lastIndexOf('t');
		Matcher m = Pattern.compile("t(\\d+)\\s*=\\s*0;").matcher(
				temps.substring(last_t, (int) temps.length()));
		if (m.matches())
			return Integer.parseInt(m.group(1));

		return Properties.__temps__;
	}

	private void execute(String metodo) {
		boolean match = false;
		Integer p = this.metodos.get(metodo);
		boolean f = false;

		if (p != null) {
			ptr = p;
			f = true;
		}

		while (ptr < fsize && !this.stop) {
			String line = nextLine();
			match = false;
			ptr = ptr + line.getBytes().length;
			next: for (int i = 0; i < patrones.length && !this.stop; i++) {
				m = patrones[i].matcher(line.trim());
				if (m.matches()) {
					match = true;
					if (!f) {
						if (i == 9) {
							String met = m.group(1) + "()";
							metodos.put(met, ptr);
							if (met.equals(metodo))
								f = true;
						}

						if (i == 10)
							etiquetas.put(m.group(1), ptr);

						break next;
					}

					switch (i) {
					case 0:
						setValue(m.group(1),
								aritmetica(m.group(2), m.group(6), m.group(7)));
						break next;
					case 1:
						setValue(m.group(1), getValue(m.group(2)) * -1);
						break next;
					case 2:
					case 3:
						setValue(m.group(1), getValue(m.group(2)));
						break next;
					case 4:
						setValue(m.group(1), getValue(m.group(7)));
						break next;
					case 5:
						String x = m.group(1);
						String op = m.group(5);
						String y = m.group(6);
						String lbl = m.group(10);
						boolean cmp = relacional(x, op, y);
						if (cmp)
							mover(metodo, lbl);
						break next;
					case 6:
						String operation = m.group(1);
						String format = m.group(2);
						String arg = m.group(3);
						Double value = getValue(arg);
						String string = null;
						if (format.equals("%f"))
							string = String.format(format, value);
						else if (format.equals("%c"))
							string = String.format(format,
									(char) value.intValue());
						else
							string = String.format(format, value.intValue());

						if (operation.equals("fprint")
								|| operation.equals("fprintf")) {
							if (Math.abs(stack[this.p.intValue()] - 0) < Properties.__epsilon__)
								this.buff.append(string);
							else {
								this.buff.setLength(0);
								this.buff.append(string);
							}
						} else
							consola.append(string);

						metodos.put(m.group(1), ptr);
						break next;
					case 7:// goto
						mover(metodo, m.group(1));
						break next;
					case 8:
						break next;
					case 9:
						metodos.put(m.group(1) + "()", ptr);
						break next;
					case 10:
						etiquetas.put(m.group(1), ptr);
						break next;
					case 11:
						return;
					case 12:
						Integer __ptr__ = ptr;
						String llamada = m.group(1);
						try {
							if (llamada.equals("_linea")) {
								Graphics2D g2D = image.createGraphics();
								g2D.setColor(new Color(
										stack[this.p.intValue() + 5].intValue(),
										stack[this.p.intValue() + 6].intValue(),
										stack[this.p.intValue() + 7].intValue()));
								g2D.drawLine(
										stack[this.p.intValue() + 1].intValue(),
										stack[this.p.intValue() + 2].intValue(),
										stack[this.p.intValue() + 3].intValue(),
										stack[this.p.intValue() + 4].intValue());
							} else if (llamada.equals("_texto")) {
								StringBuilder texto = new StringBuilder();
								int j = 0;                                                                
								Double str = stack[this.p.intValue() + 1];
                                                                System.out.println("La cadena empieza en" + str);
								while (stack[str.intValue() + j] > Properties.__epsilon__) {
									texto.append((char) stack[str.intValue()
											+ j].intValue());
									j++;
								}

								Graphics2D g2D = image.createGraphics();
								g2D.setColor(new Color(
										stack[this.p.intValue() + 4].intValue(),
										stack[this.p.intValue() + 5].intValue(),
										stack[this.p.intValue() + 6].intValue()));
								g2D.drawString(
										texto.toString(),
										stack[this.p.intValue() + 2].intValue(),
										stack[this.p.intValue() + 3].intValue());
							} else if (llamada.equals("_rectangulo")) {
								Graphics2D g2D = image.createGraphics();
								g2D.setColor(new Color(
										stack[this.p.intValue() + 5].intValue(),
										stack[this.p.intValue() + 6].intValue(),
										stack[this.p.intValue() + 7].intValue()));
								if (stack[this.p.intValue() + 8] > Properties.__epsilon__)
									g2D.fillRect(stack[this.p.intValue() + 1]
											.intValue(), stack[this.p
											.intValue() + 2].intValue(),
											stack[this.p.intValue() + 3]
													.intValue(), stack[this.p
													.intValue() + 4].intValue());
								else
									g2D.drawRect(stack[this.p.intValue() + 1]
											.intValue(), stack[this.p
											.intValue() + 2].intValue(),
											stack[this.p.intValue() + 3]
													.intValue(), stack[this.p
													.intValue() + 4].intValue());
							} else if (llamada.equals("_ovalo")) {
								Graphics2D g2D = image.createGraphics();
								g2D.setColor(new Color(
										stack[this.p.intValue() + 5].intValue(),
										stack[this.p.intValue() + 6].intValue(),
										stack[this.p.intValue() + 7].intValue()));
								if (stack[this.p.intValue() + 8] > Properties.__epsilon__)
									g2D.fillOval(stack[this.p.intValue() + 1]
											.intValue(), stack[this.p
											.intValue() + 2].intValue(),
											stack[this.p.intValue() + 3]
													.intValue(), stack[this.p
													.intValue() + 4].intValue());
								else
									g2D.drawOval(stack[this.p.intValue() + 1]
											.intValue(), stack[this.p
											.intValue() + 2].intValue(),
											stack[this.p.intValue() + 3]
													.intValue(), stack[this.p
													.intValue() + 4].intValue());
							} else if (llamada.equals("_arco")) {
								Graphics2D g2D = image.createGraphics();
								g2D.setColor(new Color(
										stack[this.p.intValue() + 7].intValue(),
										stack[this.p.intValue() + 8].intValue(),
										stack[this.p.intValue() + 9].intValue()));
								if (stack[this.p.intValue() + 10] > Properties.__epsilon__)
									g2D.fillArc(stack[this.p.intValue() + 1]
											.intValue(), stack[this.p
											.intValue() + 2].intValue(),
											stack[this.p.intValue() + 3]
													.intValue(), stack[this.p
													.intValue() + 4].intValue(),stack[this.p
													.intValue() + 5].intValue(), stack[this.p
													.intValue() + 6].intValue());
								else                                                                    
                                    g2D.drawArc(stack[this.p.intValue() + 1]
											.intValue(), stack[this.p
											.intValue() + 2].intValue(),
											stack[this.p.intValue() + 3]
													.intValue(), stack[this.p
													.intValue() + 4].intValue(),stack[this.p
													.intValue() + 5].intValue(), stack[this.p
													.intValue() + 6].intValue());
							} else if (llamada.equals("_poligono")) {
								int dim = new Double(
										stack[this.p.intValue() + 7])
										.intValue();
								int vectX = new Double(
										stack[this.p.intValue() + 1])
										.intValue();
								int vectY = new Double(
										stack[this.p.intValue() + 2])
										.intValue();
								int posX[] = new int[dim];
								int posY[] = new int[dim];

								for (int k = 0; k < dim; k++) {
									posX[k] = stack[vectX + k].intValue();
									posY[k] = stack[vectY + k].intValue();
								}

								Graphics2D g2D = image.createGraphics();
								g2D.setColor(new Color(
										stack[this.p.intValue() + 3].intValue(),
										stack[this.p.intValue() + 4].intValue(),
										stack[this.p.intValue() + 5].intValue()));
								if (stack[this.p.intValue() + 6] > Properties.__epsilon__)
									g2D.fillPolygon(posX, posY, dim);
								else
									g2D.drawPolygon(posX, posY, dim);
							} else if (llamada.equals("_lienzo")) {
								Graphics2D g2D = image.createGraphics();
								g2D.setBackground(new Color(
										stack[this.p.intValue() + 3].intValue(),
										stack[this.p.intValue() + 4].intValue(),
										stack[this.p.intValue() + 5].intValue()));
								this.width = stack[this.p.intValue() + 1]
										.intValue();
								this.height = stack[this.p.intValue() + 1]
										.intValue();
							} else {
								if (Properties.__pusht__)
									pusha();
								execute(llamada + "()");
								if (Properties.__pusht__)
									popa();
							}
						} catch (Exception e) {
							// TODO: handle exception
							errores.append(e.getMessage());
                                                        e.printStackTrace();
						}
						ptr = __ptr__;

						break next;
					}

				}
			}
			if (!match)
				System.err.println("Linea ignorada: " + line);
		}
	}

	private void writeFile() {
		try {
			FileUtils.write(new File(Properties.__path__), this.buff,
					Charset.defaultCharset());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("No se pudo generar el archivo...");
		}
		System.out.println(this.buff.toString());
	}

	private String nextLine() {
		int newline = file.indexOf(nl, ptr);
		if (newline == -1)
			return file.substring(ptr, (int) fsize);
		return file.substring(ptr, newline + 1);
	}

	private void mover(String metodo, String label) {
		Integer p = this.etiquetas.get(label);
		if (p == null) {
			for (int i = ptr; i < fsize; i++) {
				String line = nextLine();
				ptr = ptr + line.getBytes().length;
				m = patrones[10].matcher(line.trim());
				if (m.matches()) {
					this.etiquetas.put(m.group(1), ptr);
					if (m.group(1).equals(label))
						return;
				}

				m = patrones[11].matcher(line.trim());
				if (m.matches()) {
					i = this.metodos.get(metodo);
					continue;
				}
			}
		} else
			ptr = p;
	}

	private Double aritmetica(String expr1, String op, String expr2) {
		if (op.equals("+"))
			return getValue(expr1) + getValue(expr2);
		if (op.equals("-"))
			return getValue(expr1) - getValue(expr2);
		if (op.equals("*"))
			return getValue(expr1) * getValue(expr2);
		if (op.equals("/"))
			return getValue(expr1) / getValue(expr2);
		if (op.equals("%"))
			return getValue(expr1) % getValue(expr2);
		System.out
				.println("No se pudo calcular el valor " + expr1 + op + expr2);
		return Double.NaN;
	}

	private boolean relacional(String expr1, String op, String expr2) {
		if (op.equals("!="))
			return Math.abs(getValue(expr1) - getValue(expr2)) >= Properties.__epsilon__;
		if (op.equals("=="))
			return Math.abs(getValue(expr1) - getValue(expr2)) < Properties.__epsilon__;
		if (op.equals(">"))
			return getValue(expr1) > getValue(expr2);
		if (op.equals(">="))
			return getValue(expr1) > getValue(expr2)
					|| Math.abs(getValue(expr1) - getValue(expr2)) < Properties.__epsilon__;
		if (op.equals("<"))
			return getValue(expr1) < getValue(expr2);
		if (op.equals("<="))
			return getValue(expr1) < getValue(expr2)
					|| Math.abs(getValue(expr1) - getValue(expr2)) < Properties.__epsilon__;
		System.out.println("No se pudo calcular el valor booleano de " + expr1
				+ op + expr2);
		return false;
	}

	private Double getValue(String expr) {
		m = this.p_p.matcher(expr);
		if (m.matches())
			return p;
		m = this.p_h.matcher(expr);
		if (m.matches())
			return h;
		m = this.p_tmp.matcher(expr);
		if (m.matches())
			return temps[Double.valueOf(m.group(1)).intValue()];

		m = this.p_num.matcher(expr);
		if (m.matches())
			return Double.valueOf(m.group(1)
					+ (m.group(2) == null ? "" : m.group(2)));

		m = this.p_array.matcher(expr);
		if (m.matches()) {
			String g1 = m.group(1);
			Double index = getValue(m.group(2));
			if (g1.equals("stack"))
				return stack[index.intValue()];
			else
				return heap[index.intValue()];
		}

		System.out.println("No se pudo obtener el valor de " + expr);
		return Double.NaN;
	}

	private void setValue(String expr, Double value) {
		m = this.p_p.matcher(expr);
		if (m.matches()) {
			p = value;
			return;
		}
		m = this.p_h.matcher(expr);
		if (m.matches()) {
			h = value;
			return;
		}
		m = this.p_tmp.matcher(expr);
		if (m.matches()) {
			temps[Double.valueOf(m.group(1)).intValue()] = value;
			return;
		}

		m = this.p_num.matcher(expr);
		if (m.matches()) {
			System.err.println("No se puede asignar un valor a un numero");
			return;
		}

		m = this.p_array.matcher(expr);
		if (m.matches()) {
			String g1 = m.group(1);
			Double index = getValue(m.group(2));
			if (g1.equals("stack"))
				stack[index.intValue()] = value;
			else
				heap[index.intValue()] = value;

			return;
		}

		System.out.println("No se pudo asignar el valor a " + expr);
	}

	public void dump() {
		for (int i = 0; i < 100; i++) {
			System.out.print(String.format("%d(%c, %d) | ", i,
					(char) stack[i].intValue(), stack[i].intValue()));
		}
		System.out.println();
		System.out.println("------------------------------");
		for (int i = 0; i < 100; i++) {
			System.out.print(String.format("%d(%c, %d) | ",
					Generador.offsetHeap + i, (char) stack[Generador.offsetHeap
							+ i].intValue(),
					stack[Generador.offsetHeap + i].intValue()));
		}
	}

}
