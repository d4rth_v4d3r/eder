package org.compi2;

import java.awt.Desktop;
import java.io.File;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

public class Proyecto {
	private ArrayList<String> archivos;
	private String workspace;
	private String nombre;
	private String archivoPrincipal;
	public static String dotPath = "C:\\Program Files (x86)\\Graphviz 2.28\\bin\\dot.exe";

	public Proyecto(String workspace, String nombre) {
		this.archivos = new ArrayList<String>();
		this.nombre = nombre;
		this.archivoPrincipal = null;
		this.workspace = workspace;
	}

	public static Proyecto abrirProyecto(String path) {
		try {
			SintacticoP s = new SintacticoP(path);
			s.parse();
			return s.p;
		} catch (Exception e) {
			// TODO: handle exception
			SintacticoP.mensaje = e.getMessage();
			return null;
		}
	}

	public boolean compilar(String archivo) {
		this.setArchivoPrincipal(archivo);
		this.guardarConfiguracion();
		try {
			Sintactico.workspace = this.workspace
					+ System.getProperty("file.separator");
			Sintactico s = new Sintactico(archivo);
			s.parse();

			// if(Errores.mostrarErrores())

			// Tipo.imprimirData();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean mostrarTabla() {
		try {
			String html = IOUtils.toString(Main.class.getClassLoader()
					.getResourceAsStream("resources/html2.txt"), Charset
					.defaultCharset());
			File ts = new File(this.workspace
					+ System.getProperty("file.separator") + "ts.html");
			FileUtils.write(ts, String.format(html, "Tabla de Simbolos",
					"Resultados", Sintactico.clases.toHTML().toString()),
					Charset.defaultCharset());
			Desktop.getDesktop().open(ts);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean mostrarErroresLexicos() {
		return mostrarErrores(Errores.getErroresLexicos(), "Errores Lexicos",
				"Reporte de Errores Lexicos", true);
	}

	public boolean mostrarErroresSintacticos() {
		return mostrarErrores(Errores.getErroresSintacticos(),
				"Errores Sintacticos", "Reporte de Errores Sintacticos", true);
	}

	public boolean mostrarErroresSemanticos() {
		return mostrarErrores(Errores.getErroresSemanticos(),
				"Errores Semanticos", "Reporte de Errores Semanticos", true);
	}

	public boolean mostrarWarnings() {
		return mostrarErrores(Errores.getInformacion(), "Warnings",
				"Reporte de Advertencias", false);
	}

	private boolean mostrarErrores(ArrayList<String> errores, String titulo,
			String header, boolean abrir) {
		if (errores.size() == 0)
			return false;
		try {
			String html = IOUtils.toString(Main.class.getClassLoader()
					.getResourceAsStream("resources/html.txt"), Charset
					.defaultCharset());
			File ts = new File(this.workspace
					+ System.getProperty("file.separator") + titulo + ".html");
			StringBuilder listado = new StringBuilder();
			for (String error : errores) {
				listado.append(error);
			}
			FileUtils.write(ts, String.format(html, titulo, header, listado),
					Charset.defaultCharset());
			if (abrir)
				Desktop.getDesktop().open(ts);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean isPrincipal(String archivo) {
		return this.archivoPrincipal != null
				&& this.archivoPrincipal.equalsIgnoreCase(archivo);
	}

	public String leerArchivo(String archivo) {
		try {
			return FileUtils.readFileToString(
					new File(workspace + System.getProperty("file.separator")
							+ archivo), Charset.defaultCharset());
		} catch (Exception e) {
			// TODO: handle exception
			return "";
		}
	}

	public boolean agregarArchivo(String archivo, boolean crear) {
		for (String a : archivos) {
			if (archivo.equals(a))
				return false;
		}

		this.archivos.add(archivo);
		if (crear)
			return crearArchivo(archivo);
		return true;
	}

	private boolean crearArchivo(String archivo) {
		try {
			File frc = new File(workspace
					+ System.getProperty("file.separator") + archivo);
			frc.createNewFile();
			guardarConfiguracion();
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(workspace);
			e.printStackTrace();
		}
		return true;

	}

	public boolean crear() {
		try {

			File folder = new File(workspace);

			if (folder.exists()) {
				return false;
			}

			folder.mkdirs();
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(workspace);
			e.printStackTrace();
		}
		return true;
	}

	private void guardarConfiguracion() {
		guardarArchivo(nombre + ".pj", getXml());
	}

	public void guardarArchivo(String nombre, String contenido) {
		try {
			File pj = new File(workspace + System.getProperty("file.separator")
					+ nombre);

			if (pj.exists())
				pj.delete();

			pj.createNewFile();

			PrintWriter pw = new PrintWriter(pj);
			pw.print(contenido);
			pw.flush();
			pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setArchivoPrincipal(String nombre) {
		this.archivoPrincipal = nombre;
	}

	public String getWorkspace() {
		return this.workspace;
	}

	public String getNombre() {
		return this.nombre;
	}

	public String getXml() {
		StringBuilder proyecto = new StringBuilder();
		proyecto.append(String.format("<Proyecto nombre=\"%s\" ruta=\"%s\">%s",
				nombre, workspace, System.getProperty("line.separator")));

		proyecto.append(String.format("\t<Archivos>%s",
				System.getProperty("line.separator")));
		for (String archivo : this.archivos) {
			proyecto.append(String.format("\t\t<Archivo nombre=\"%s\" />%s",
					archivo, System.getProperty("line.separator")));
		}
		proyecto.append(String.format("\t</Archivos>%s",
				System.getProperty("line.separator")));
		proyecto.append(String.format("\t<Principal>%s",
				System.getProperty("line.separator")));
		if (archivoPrincipal != null)
			proyecto.append(String.format("\t\t<Archivo nombre=\"%s\" />%s",
					archivoPrincipal, System.getProperty("line.separator")));
		proyecto.append(String.format("\t</Principal>%s",
				System.getProperty("line.separator")));
		proyecto.append(String.format("</Proyecto>",
				System.getProperty("line.separator")));

		return proyecto.toString();
	}

	public void generarDOT(String nombreArchivo) {
		try {
			String fileInputPath = this.workspace
					+ System.getProperty("file.separator") + nombreArchivo;
			String fileOutputPath = this.workspace
					+ System.getProperty("file.separator") + nombreArchivo
					+ ".jpg";

			String tParam = "-Tjpg";
			String tOParam = "-o";

			String[] cmd = new String[5];
			cmd[0] = dotPath;
			cmd[1] = tParam;
			cmd[2] = fileInputPath;
			cmd[3] = tOParam;
			cmd[4] = fileOutputPath;

			Runtime rt = Runtime.getRuntime();
			rt.exec(cmd);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
		}
	}

	public ArrayList<String> getArchivos() {
		return this.archivos;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.getNombre();
	}
}
