package org.compi2;

import java.io.File;
import java.util.ArrayList;

import org.apache.commons.io.FileUtils;

@SuppressWarnings("unchecked")
public class Generador {

    private static int temporales = 0;
    private static int etiquetas = 0;
    public static final int offsetHeap = 10000;
    private static String indentacion;
    private static StringBuilder codigo, codigo2;
    public static boolean escribir = false, almacenar = false;
    public static String nombreArchivo;
    public static File salida;
    public static boolean ignorarErrorDimensiones = true;

    public static void inicializar(String salida3D) {
        salida = new File(Sintactico.workspace + salida3D);
        try {
            if (salida.exists()) {
                salida.delete();
            } else {
                salida.createNewFile();
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        escribir = true;
        indentacion = "";
        begin("__init__");
        set(s(), "0", "Apuntamos a la region donde estara la memoria de pila");
        set(h(), String.valueOf(offsetHeap),
                "Apuntamos a la region donde estara la memoria dinamica");
        end();
        intToString();
        booleanToString();
        concat();
        codigo = new StringBuilder();
        codigo2 = new StringBuilder();
        escribir = false;
    }

    public static int obtenerTemporales() {
        return temporales;
    }

    public static String resetearCodigo() {
        String codigo1 = codigo.toString();
        codigo = new StringBuilder();
        return codigo1;
    }

    public static String resetearCodigo2() {
        String codigo1 = codigo2.toString();
        codigo2 = new StringBuilder();
        return codigo1;
    }

    public static String getCodigo() {
        return codigo.toString();
    }

    public static String getCodigo2() {
        return codigo2.toString();
    }

    /**
     * Metodo que devuele el puntero en el stack.
     *
     * @return La cadena que representa a la variable de puntero.
     */
    public static String s() {
        return "p";
    }

    /**
     * Metodo que devuele el puntero en el heap.
     *
     * @return La cadena que representa a la variable de puntero.
     */
    public static String h() {
        return "h";
    }

    /**
     *
     * @return Metodo que devuelve la cadena que representa a un valor en el
     * stack.
     */
    public static String stack(String posicion) {
        return String.format("stack[%s]", posicion);
    }

    /**
     *
     * @return Metodo que devuelve la cadena que representa a un valor en el
     * heap.
     */
    public static String heap(String posicion) {
        /*
         * String temp = generaTemporal(); set(temp, posicion,
         * Enumeraciones.TAritmeticos.SUMA.toString(),
         * String.valueOf(offsetHeap),
         * "Desplazamos hacia el lugar de la memoria dinamica");
         */
        return String.format("stack[%s]", posicion);
    }

    /**
     * Devuelve un temporal y aumenta la cuenta
     *
     * @return El temporal creado.
     */
    public static String generaTemporal() {
        return String.format("t%d", temporales++);
    }

    /**
     * Devuelve una etiqueta y aumenta la cuenta
     *
     * @return La etiqueta creada.
     */
    public static String generaEtiqueta() {
        return String.format("L%d", etiquetas++);
    }

    /**
     * Genera codigo para una llamada hacia un metodo
     *
     * @param metodo El nombre del metodo
     */
    public static String call(String metodo) {
        return write(String.format("call %s();", metodo));
    }

    /**
     * Genera codigo para poner una etiqueta de salto.
     *
     * @param etiqueta El nombre del salto.
     */
    public static String label(String etiqueta) {
        return write(String.format("%s:", etiqueta));
    }

    /**
     * Genera codigo para saltar (goto) a una etiqueta.
     *
     * @param etiqueta El nombre de la etiqueta.
     */
    public static String jump(String etiqueta) {
        return write(String.format("goto %s;", etiqueta));
    }

    /**
     * Genera codigo para una llamada hacia un metodo
     *
     * @param metodo El nombre del metodo
     * @param comentario Comentario que describe a la llamada de la funcion
     *
     */
    public static String call(String metodo, String comentario) {
        return write(String.format("call %s(); // %s", metodo, comentario));
    }

    /**
     * Genera codigo para poner una etiqueta de salto.
     *
     * @param etiqueta El nombre del salto.
     * @param comentario Comentario que describe el origen de la etiqueta
     */
    public static String label(String etiqueta, String comentario) {
        return write(String.format("%s: // %s", etiqueta, comentario));
    }

    /**
     * Genera codigo para saltar (goto) a una etiqueta.
     *
     * @param etiqueta El nombre de la etiqueta.
     * @param comentario Comenario para describir el salto
     */
    public static String jump(String etiqueta, String comentario) {
        return write(String.format("goto %s; // %s", etiqueta, comentario));
    }

    /**
     * Genera codigo para una asignacion
     *
     * @param x La variable a la que se va a asignar
     * @param y El valor a asignar
     */
    public static String set(String x, String y) {
        return write(String.format("%s = %s;", x, y));
    }

    /**
     * Genera codigo para una asignacion con una expresion
     *
     * @param x La variable a la que se va a asignar
     * @param y El primer operando de la expresion. Si es una operacion unaria
     * puede ser vacio.
     * @param operador El operador
     * @param z El segundo operando de la expresion
     */
    public static String set(String x, String y, String operador, String z) {
        return write(String.format("%s = %s %s %s;", x, y, operador, z));
    }

    /**
     * Genera codigo para una asignacion
     *
     * @param x La variable a la que se va a asignar
     * @param y El valor a asignar
     * @param comentario Comentario para la linea de codigo
     */
    public static String set(String x, String y, String comentario) {
        return write(String.format("%s = %s; // %s", x, y, comentario));
    }

    /**
     * Genera codigo para una asignacion con una expresion
     *
     * @param x La variable a la que se va a asignar
     * @param y El primer operando de la expresion. Si es una operacion unaria
     * puede ser vacio.
     * @param operador El operador
     * @param z El segundo operando de la expresion
     * @param comentario Comentario para la linea de codigo
     */
    public static String set(String x, String y, String operador, String z,
            String comentario) {
        return write(String.format("%s = %s %s %s; // %s", x, y, operador, z,
                comentario));
    }

    public static String jumpIf(String x, String operadorRelacional, String y,
            String etiqueta) {
        return write(String.format("if %s %s %s goto %s;", x, operadorRelacional, y,
                etiqueta));
    }

    public static String jumpIf(String x, String operadorRelacional, String y,
            String etiqueta, String comentario) {
        return write(String.format("if %s %s %s goto %s; // %s", x,
                operadorRelacional, y, etiqueta, comentario));
    }

    /**
     * Imprimee una linea de comentario en el codigo
     *
     * @param comentario El comentario a escribir
     */
    public static String debug(String comentario) {
        return write(String.format("// -- %s -- ", comentario));
    }

    /**
     * Imprime comentarios en el codigo, hay que hacer dump para un nuevo salto
     * de linea
     *
     * @param comentario El comentario a escribir
     */
    public static String debug2(String comentario) {
        return write2(String.format("%s", comentario));
    }

    public static String debug2() {
        return write("");
    }

    /**
     * Genera codigo de declaracion de un metodo. Codigo de cabecera.
     *
     * @param metodo El nombre del metodo.
     */
    public static String begin(String metodo) {
        write(String.format("method %s() {", metodo));
        indentacion = "\t";
        return String.format("method %s() {", metodo);
    }

    /**
     * Genera codigo para terminar un ambiente.
     */
    public static String end() {
        indentacion = "";
        return write(String.format("}")) + debug2();
    }

    public static String write(String codigo) {
    	StringBuilder ret = new StringBuilder(); 
    	
        if (almacenar) {
            Generador.codigo2.append(codigo
                    + System.getProperty("line.separator"));
            ret.append(codigo
                    + System.getProperty("line.separator"));
        } else if (escribir) {
            String[] lineas = codigo
                    .split(System.getProperty("line.separator"));
            for (String string : lineas) {
                // System.out.println(indentacion + string);
                try {
                    FileUtils.write(
                            salida,
                            indentacion + string
                            + System.getProperty("line.separator"),
                            true);
                    ret.append(indentacion + string
                            + System.getProperty("line.separator"));
                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }

            }
        } else {
            Generador.codigo.append(codigo
                    + System.getProperty("line.separator"));
            ret.append(codigo
                    + System.getProperty("line.separator"));
        }
        
        return ret.toString();
    }

    public static String write2(String codigo) {
        if (almacenar) {
            Generador.codigo2.append(codigo);
        } else if (escribir) {
            // System.out.print(codigo);
            try {
                FileUtils.write(salida, codigo, true);
            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }
        } else {
            Generador.codigo.append(codigo);
        }
        
        return codigo;
    }

    private static void intToString() {
        String direccion = generaTemporal();
        String numero = generaTemporal();
        String base = generaTemporal();
        String etiquetaInicio1 = generaEtiqueta();
        String etiquetaSalida1 = generaEtiqueta();
        String etiquetaSalida2 = generaEtiqueta();
        String modulo = generaTemporal();
        String caracter = generaTemporal();
        String dirRetorno = generaTemporal();
        String retorno = generaTemporal();

        begin("__nString__");

        set(direccion, s(), "+", "0");
        set(numero, stack(direccion), "Sacamos el numero a convertir");
        set(base, "1", "Inicializamos el contador base de digitos");
        set(retorno, h(), "Guardamos la direccion de la cadena");

        label(etiquetaInicio1);
        set(modulo, numero, "/", base);
        jumpIf(modulo, "<", "10", etiquetaSalida1,
                "Si es menor a 10 tenemos el numero base");
        set(base, base, "*", "10", "Sino, tenemos que incrementar la base");
        jump(etiquetaInicio1);

        label(etiquetaSalida1);
        jumpIf(base, "<", "1", etiquetaSalida2);
        set(caracter, numero, "/", base,
                "Dividimos el numero entre la base y guardamos el cociente");
        set(caracter, caracter, "+", "48");
        set(heap(h()), caracter);
        set(h(), h(), "+", "1");
        set(numero, numero, "%", base);
        set(base, base, "/", "10");
        jump(etiquetaSalida1);

        label(etiquetaSalida2);
        String salida = generaEtiqueta();
        etiquetaSalida1 = generaEtiqueta();
        String j = generaTemporal();
        jumpIf(numero, "==", "0", salida,
                "Si es menor a 10 tenemos el numero base");
        set(heap(h()), "46");
        set(h(), h(), "+", "1");
        set(j, "0");
        label(etiquetaSalida1);
        jumpIf(j, ">=", "6", salida);
        set(caracter, numero, "/", base,
                "Dividimos el numero entre la base y guardamos el cociente");
        set(caracter, caracter, "+", "48");
        jumpIf(caracter, ">=", "58", salida);
        set(heap(h()), caracter);
        set(h(), h(), "+", "1");
        // write("print(\"%f\"," + numero + ");");
        // write("print(\"%f\"," + base + ");");
        set(numero, numero, "%", base);
        set(base, base, "/", "10");
        set(j, j, "+", "1");
        jump(etiquetaSalida1);

        label(salida);
        set(heap(h()), "0", "Guardamos el byte de fin en memoria");
        set(h(), h(), "+", "1", "Movemos el puntero");
        set(dirRetorno, s(), "+", "0");
        set(stack(dirRetorno), retorno);

        end();
    }

    private static void booleanToString() {
        String etiquetaFalsa = generaEtiqueta();

        String direccion = generaTemporal();
        String booleano = generaTemporal();
        String retorno = generaTemporal();

        begin("__bString__");
        set(direccion, s(), "+", "0");
        set(booleano, stack(direccion), "Sacamos el numero a convertir");
        set(retorno, h(), "Guardamos la direccion de la cadena");
        jumpIf(booleano, "==", "0", etiquetaFalsa);

        set(heap(h()), String.valueOf((int) 't'));
        set(h(), h(), "+", "1");
        set(heap(h()), String.valueOf((int) 'r'));
        set(h(), h(), "+", "1");
        set(heap(h()), String.valueOf((int) 'u'));
        set(h(), h(), "+", "1");
        set(heap(h()), String.valueOf((int) 'e'));
        set(h(), h(), "+", "1");
        String salida = generaEtiqueta();
        jump(salida);
        label(etiquetaFalsa);
        set(heap(h()), String.valueOf((int) 'f'));
        set(h(), h(), "+", "1");
        set(heap(h()), String.valueOf((int) 'a'));
        set(h(), h(), "+", "1");
        set(heap(h()), String.valueOf((int) 'l'));
        set(h(), h(), "+", "1");
        set(heap(h()), String.valueOf((int) 's'));
        set(h(), h(), "+", "1");
        set(heap(h()), String.valueOf((int) 'e'));
        set(h(), h(), "+", "1");
        label(salida);
        set(heap(h()), "0", "Guardamos el byte de fin en memoria");
        set(h(), h(), "+", "1", "Movemos el puntero");

        direccion = generaTemporal();
        set(direccion, s(), "+", "0");
        set(stack(direccion), retorno);
        end();
    }

    public static String charToString(String caracter) {
        String retorno = generaTemporal();

        set(retorno, h(), "Guardamos la direccion de la cadena");
        set(heap(h()), caracter);
        set(h(), h(), "+", "1");
        set(heap(h()), "0", "Guardamos el byte de fin en memoria");
        set(h(), h(), "+", "1", "Movemos el puntero");

        return retorno;
    }

    private static void concat() {
        String retorno = generaTemporal();

        begin("__concat__");
        set(retorno, h(), "Guardamos la direccion de la cadena");
        String direccion = generaTemporal();
        set(direccion, s(), "+", "0");
        String cadena1 = generaTemporal();
        set(cadena1, stack(direccion));
        String etiquetaInicio1 = generaEtiqueta();
        String etiquetaInicio2 = generaEtiqueta();

        label(etiquetaInicio1);
        String caracter = generaTemporal();
        set(caracter, heap(cadena1));
        jumpIf(caracter, "==", "0", etiquetaInicio2);
        set(heap(h()), caracter);
        set(cadena1, cadena1, "+", "1");
        set(h(), h(), "+", "1");
        jump(etiquetaInicio1);

        label(etiquetaInicio2);
        direccion = generaTemporal();
        set(direccion, s(), "+", "1");
        String cadena2 = generaTemporal();
        set(cadena2, stack(direccion));
        etiquetaInicio2 = generaEtiqueta();
        etiquetaInicio1 = generaEtiqueta();
        caracter = generaTemporal();

        label(etiquetaInicio2);
        set(caracter, heap(cadena2));
        jumpIf(caracter, "==", "0", etiquetaInicio1);
        set(heap(h()), caracter);
        set(cadena2, cadena2, "+", "1");
        set(h(), h(), "+", "1");
        jump(etiquetaInicio2);

        label(etiquetaInicio1);
        set(heap(h()), "0", "Guardamos el byte de fin en memoria");
        set(h(), h(), "+", "1", "Movemos el puntero");
        String dirRetorno = generaTemporal();
        set(dirRetorno, s(), "+", "0");
        set(stack(dirRetorno), retorno);
        end();
    }

    /**
     * Devuelve la direccion del simbolo s.
     *
     * @param objetoThis Si el simbolo s es un atributo de un objeto, entonces
     * este sera el offset, sino el offset es una posicion relativa con el
     * puntero de heap o stack.
     * @param s El simbolo al que se le va a calcular el desplazamiento
     * @return
     */
    public static String obtenerPosicion(String objetoThis, Simbolo s,
            int linea, int columna) {
        if (s.getTipo2() == Simbolo.LITERAL) {
            System.err.println("Una constante no tiene posicion.");
            return s.getId();
        }

        String posicion = generaTemporal();
        set(posicion, objetoThis == null ? (s.getLugar() == Simbolo.HEAP ? h()
                : s()) : objetoThis, "+", String.valueOf(s.getPos()),
                "Se recupera la direccion de '" + s.getId() + "'.");
        if (s.getTipo2() == Simbolo.REFERENCIA) {
            if (s.getLugar() == null) {
                Errores.errorSemantico(
                        "Las variables por referencia siempre estan en el Stack o en el Heap.",
                        linea, columna);
            }
            String posicion2 = generaTemporal();
            set(posicion2, stack(posicion),
                    "Devolvemos el valor dado que es una variable por referencia: "
                    + s.getId());
            return posicion2;
        }
        return posicion;
    }

    public static String obtenerPosicionArreglo(String objetoThis,
            Simbolo arreglo, ArrayList<Simbolo> indices, int linea, int columna) {
        StringBuilder firma = new StringBuilder();
        for (Simbolo simbolo : indices) {
            firma.append("/" + simbolo.getTipo());
        }

        if (!validarArreglo(firma.toString(), arreglo, linea, columna)) {
            return "<error>";
        }
        ArrayList<Integer> dimensionesArreglo = (ArrayList<Integer>) arreglo
                .getExtra();
        arreglo.setId(obtenerPosicion(objetoThis, arreglo, linea, columna));
        String posicionReal = obtenerValor(arreglo);
        String posicion = obtenerValor(indices.get(0)), tamanoDimension = "", direccion = "";

        for (int i = 1; i < dimensionesArreglo.size(); ++i) {
            tamanoDimension = generaTemporal();

            set(tamanoDimension, posicion, "*", dimensionesArreglo.get(i)
                    .toString(), "Calculamos el offset");
            String posicionRelativa = obtenerValor(indices.get(i));
            direccion = generaTemporal();
            set(direccion, tamanoDimension, "+", posicionRelativa,
                    "Encontramos la posicion en el arreglo.");
            posicion = direccion;

        }
        String direccionReal = generaTemporal();
        set(direccionReal, posicionReal, "+", posicion,
                "Encontramos la direccion final.");

        return direccionReal;
    }

    public static String obtenerObjetoThis() {
        String direccionThis = generaTemporal();
        String objetoThis = generaTemporal();
        set(direccionThis, s(), "+", "0");
        set(objetoThis, stack(direccionThis), "Recuperamos this...");

        return objetoThis;
    }

    public static void setearRetorno(Simbolo s, int linea, int columna) {
        String direccionThis = generaTemporal();
        set(direccionThis, s(), "+", "0");
        set(stack(direccionThis), obtenerValor(s), "Ponemos return...");
    }

    public static String instanciar(String contexto, String tamanoClase,
            Simbolo s, ArrayList<Simbolo> parametrosLlamada, int linea,
            int columna) {
        String instancia = generaTemporal();
        set(instancia, h());
        set(h(), h(), "+", tamanoClase);
        // String direccionRetorno = generaTemporal();
        // set(direccionRetorno, s(), "+", contexto);
        // String retorno = generaTemporal();
        // ADVERTENCIA: COMO ASUMIMOS QUE UN METODO NO TIENE UNA DIRECCION
        // ESPECIFICA, RETORNAMOS EL VALOR...
        // set(retorno, stack(direccionRetorno), "Recuperamos el regreso.... ");
        return invocar(contexto, instancia, s, parametrosLlamada, linea,
                columna, Sintactico.clases.get(s.getAmbiente()));
    }

    public static void herenciaConstructores(MisSimbolos ts, String firma) {
        MisSimbolos aux = ts.getPadre();

        while (aux != null) {
            Simbolo s = aux.get("met_" + aux.getClase() + firma);

            if (s != null) {
                Generador.call(s.getAmbiente() + "_" + s.getId());
            }
            aux = aux.getPadre();
        }
    }

    public static String invocar(String contexto, String objetoThis, Simbolo s,
            ArrayList<Simbolo> parametrosLlamada, int linea, int columna, MisSimbolos clase) {
        String direccionThis = generaTemporal();
        set(direccionThis, s(), "+", contexto);
        set(stack(direccionThis), objetoThis,
                "Pasamos la referencia de this...");
        String desplazamiento = generaTemporal();
        set(desplazamiento, direccionThis, "+", "1");

        String firma = setearParametros(contexto, desplazamiento, parametrosLlamada,
                (ArrayList<Simbolo>) s.getExtra(), linea, columna, s.getId());
        set(s(), s(), "+", contexto, "Cambiamos de ambientes");
        if (clase != null) {
            herenciaConstructores(clase, firma);
        }
        call(s.getAmbiente() + "_" + s.getId());
        set(s(), s(), "-", contexto, "Regresamos de ambientes");

        if (s.getTipo() != Simbolo.VACIO) {
            String direccionRetorno = generaTemporal();
            set(direccionRetorno, s(), "+", contexto);
            String retorno = generaTemporal();
            // ADVERTENCIA: COMO ASUMIMOS QUE UN METODO NO TIENE UNA DIRECCION
            // ESPECIFICA, RETORNAMOS EL VALOR...
            set(retorno, stack(direccionRetorno),
                    "Recuperamos el regreso de la funcion.... ");
            return retorno;
        }
        return "<error>";
    }

    public static String invocar(String contexto, String api,
            ArrayList<Simbolo> parametrosLlamada,
            ArrayList<Simbolo> parametrosMetodo, int linea, int columna) {
        String desplazamiento = generaTemporal();
        set(desplazamiento, s(), "+", contexto);

        setearParametros(contexto, desplazamiento, parametrosLlamada,
                parametrosMetodo, linea, columna, api);
        set(s(), s(), "+", contexto, "Cambiamos de ambientes");
        call(api);
        set(s(), s(), "-", contexto, "Regresamos de ambientes");
        return "<error>";
    }

    public static String setearParametros(String contexto,
            String desplazamiento, ArrayList<Simbolo> parametrosLlamada,
            ArrayList<Simbolo> parametrosMetodo, int linea, int columna,
            String id) {
        StringBuffer sb = new StringBuffer();
        int tamanoStack = 0;

        for (int i = 0; i < parametrosMetodo.size(); ++i) {
            if (parametrosLlamada.get(i).getTipo2() == Simbolo.ARRAY) {
                /*
                 * Errores.errorSemantico("No se permite el paso de arrays" +
                 * " como parametros.", linea, columna); break;
                 */
            }

            String nuevoDesplazamiento = generaTemporal();
            if (parametrosMetodo.get(i).getTipo2() == Simbolo.REFERENCIA) {
                if (parametrosLlamada.get(i).getTipo2() == Simbolo.LITERAL
                        || parametrosLlamada.get(i).getTipo2() == Simbolo.METODO) {
                    Errores.errorSemantico("El parametro es un "
                            + parametrosLlamada.get(i).getTipo2()
                            + " y se necesita una referencia.", linea, columna);
                    break;
                }
            }

            if (!parametrosMetodo.get(i).getTipo()
                    .equals(parametrosLlamada.get(i).getTipo())
                    && parametrosMetodo.get(i).getTipo() != Simbolo.CADENA) {
                Errores.errorSemantico(
                        "Los tipos de parametros no coinciden con los del metodo, se espera "
                        + parametrosMetodo.get(i).getTipo()
                        + " y se encontro "
                        + parametrosLlamada.get(i).getTipo(), linea,
                        columna);
                break;
            }

            if (parametrosMetodo.get(i).getTipo() == Simbolo.CADENA
                    && parametrosLlamada.get(i).getTipo() != Simbolo.CADENA) {

                String nuevoContexto = generaTemporal();

                set(nuevoContexto, contexto, "+", String.valueOf(tamanoStack),
                        "Movemos el contexto para que no sobreescriba los parametros...");

                parametrosLlamada.get(i).setId(
                        toString(parametrosLlamada.get(i), nuevoContexto,
                                linea, columna));
                parametrosLlamada.get(i).setLugar(Simbolo.HEAP);
                parametrosLlamada.get(i).setTipo2(Simbolo.LITERAL);
            }

            if (parametrosMetodo.get(i).getTipo2() != Simbolo.REFERENCIA) {
                parametrosLlamada.get(i).setId(
                        obtenerValor(parametrosLlamada.get(i)));
            }/* else {
                if (parametrosMetodo.get(i).getTipo2() == Simbolo.REFERENCIA
                        && parametrosLlamada.get(i).getLugar() != Simbolo.STACK) {
                    Errores.errorSemantico(
                            "Las variables por referencia deben de estar en el stack.",
                            linea, columna);
                    break;
                }
            }*/

            if (id != null && id.equals("poligono")) {
                if (i == 1) {
                    try {
                        ArrayList<Integer> posX = (ArrayList<Integer>) ((Simbolo) parametrosLlamada
                                .get(0).getExtra()).getExtra();
                        ArrayList<Integer> posY = (ArrayList<Integer>) ((Simbolo) parametrosLlamada
                                .get(1).getExtra()).getExtra();
                        if (posX.size() == 0 || posY.size() == 0) {
                            Errores.errorSemantico(
                                    "Debe asignar un valor estatico a la dimension del vectores X y Y (No debe ser variable o desconocido).",
                                    linea, columna);
                            break;
                        } else if (posX.size() != posY.size()) {
                            Errores.errorSemantico(
                                    "El NUMERO de los vectores para puntos X y Y no son iguales.",
                                    linea, columna);
                            break;
                        } else if (posX.size() != 1) {
                            Errores.errorSemantico(
                                    "Se debe asignar un vector de una dimension unicamente.",
                                    linea, columna);
                            break;
                        } else if (((Number) posX.get(0)).intValue() != ((Number) posY
                                .get(0)).intValue()) {
                            Errores.errorSemantico(
                                    "El TAMANO de las dimensiones no son iguales."
                                    + posX.get(0) + "!=" + posY.get(0),
                                    linea, columna);
                            break;
                        } else {
                            Simbolo dim = new Simbolo();
                            dim.setId(String.valueOf(posX.get(0)));
                            dim.setLugar(null);
                            dim.setTipo2(Simbolo.LITERAL);
                            dim.setTipo(Simbolo.ENTERO);
                            parametrosLlamada.add(dim);
                            System.out
                                    .println("------------------ DIMENSION AGREGADA ------------------");
                            // Agregamos el parametro de llamada....
                        }
                    } catch (Exception e) {
                        Errores.errorSemantico(
                                "No se pudieron establecer las dimensiones de los vectores de puntos.",
                                linea, columna);
                        break;
                    }

                }
            }

            set(stack(desplazamiento), parametrosLlamada.get(i).getId(),
                    "Pasando el parametro ->'"
                    + parametrosMetodo.get(i).getId() + "' por "
                    + parametrosMetodo.get(i).getTipo2());

            set(nuevoDesplazamiento, desplazamiento, "+",
                    String.valueOf(parametrosMetodo.get(i).getTamano()));
            tamanoStack += parametrosMetodo.get(i).getTamano();
            desplazamiento = nuevoDesplazamiento;
            sb.append("_" + parametrosMetodo.get(i).getTipo());
        }

        return sb.toString();
    }

    private static boolean validarArreglo(String key, Simbolo arreglo,
            int linea, int columna) {
        ArrayList<Integer> dimensiones = (ArrayList<Integer>) arreglo
                .getExtra();
        if (dimensiones == null) {
            if (!ignorarErrorDimensiones) {
                Errores.errorSemantico(
                        "El arreglo no tiene dimensiones definidas...", linea,
                        columna);
            } else {
                Errores.informacion(
                        "El arreglo no tiene dimensiones definidas...", linea,
                        columna);
            }
            return ignorarErrorDimensiones;
        }

        String callParams[] = key.substring(key.indexOf("/") + 1, key.length())
                .split("/");

        if (callParams.length == dimensiones.size()) {
            for (int i = 0; i < callParams.length; i++) {
                if (!castImplicito(callParams[i], Simbolo.ENTERO)) {
                    Errores.errorSemantico("Tipo de indice invalido: "
                            + callParams[i] + ".", linea, columna);
                    return false;
                }

            }
        } else {
            if (!ignorarErrorDimensiones) {
                Errores.errorSemantico(
                        "Dimensiones invalidas: " + callParams.length
                        + " y se esperan " + dimensiones.size(), linea,
                        columna);
            } else {
                Errores.informacion(
                        "Dimensiones invalidas: " + callParams.length
                        + " y se esperan " + dimensiones.size(), linea,
                        columna);
            }
            return ignorarErrorDimensiones;
        }

        return true;
    }

    public static String obtenerValor(Simbolo s) {
        if (s.getLugar() == null || s.getTipo2() == Simbolo.LITERAL) {
            return s.getId();
        }

        String valor = generaTemporal();

        set(valor,
                s.getLugar() == Simbolo.HEAP ? heap(s.getId()) : stack(s
                        .getId()), "Obtenemos el valor de " + s.getId());

        return valor;
    }

    public static void setearValor(Simbolo x, Simbolo y, int linea, int columna) {
        if (x.getTipo() != Simbolo.VACIO
                && (x.getLugar() == null || x.getTipo2() == Simbolo.LITERAL)) {
            Errores.errorSemantico("No se puede asignar valor a una constante",
                    linea, columna);
            return;
        }

        if (y.getTipo() == Simbolo.VACIO) {
            // Errores.errorSemantico("Error de asignacion", linea, columna);
            return;
        }

        if (castImplicito(y.getTipo(), x.getTipo())) {
            set(x.getLugar() == Simbolo.HEAP ? heap(x.getId())
                    : stack(x.getId()), obtenerValor(y), "Seteamos valor a "
                    + x.getId());
        } else {
            Errores.errorSemantico("Tipos en asignacion erroneos", linea,
                    columna);
        }
    }

    public static String generarCadena(String v) {
        String tinicial = generaTemporal();
        set(tinicial,
                h(),
                "Guardamos el inicio de la cadena '"
                + v.replaceAll("\n", "@n@") + "'");

        for (int i = 0; i < v.length(); ++i) {
            set(heap(h()), String.valueOf((int) v.charAt(i)));
            set(h(), h(), "+", "1");
        }
        set(heap(h()), "0");
        set(h(), h(), "+", "1");

        return tinicial;
    }

    public static boolean tieneAtributos(Simbolo s) {
        return !(s.getTipo().equals(Simbolo.LOGICO)
                || s.getTipo().equals(Simbolo.ENTERO)
                || s.getTipo().equals(Simbolo.DECIMAL)
                || s.getTipo().equals(Simbolo.CARACTER)
                || s.getTipo().equals(Simbolo.CADENA) || s.getTipo().equals(
                        Simbolo.VACIO));
    }

    /**
     * Verifica si es posible hacer un cast implicito.
     *
     * @param claseHija La clase que se intenta castear
     * @param clasePadre La clase destino de cast
     * @return Verdadero si la conversión se permite, falso en cualquier otro
     * caso
     */
    public static boolean castImplicito(String claseHija, String clasePadre) {

        if (claseHija.equals(Simbolo.LOGICO)) {
            if (clasePadre.equals(Simbolo.LOGICO)) {
                return true;
            } else if (clasePadre.equals(Simbolo.CADENA)) {
                return true;
            }

        } else if (claseHija.equals(Simbolo.CARACTER)) {
            if (clasePadre.equals(Simbolo.CARACTER)) {
                return true;
            } else if (clasePadre.equals(Simbolo.ENTERO)) {
                return true;
            } else if (clasePadre.equals(Simbolo.DECIMAL)) {
                return true;
            } else if (clasePadre.equals(Simbolo.CADENA)) {
                return true;
            }

        } else if (claseHija.equals(Simbolo.ENTERO)) {
            if (clasePadre.equals(Simbolo.CARACTER)) {
                return false;
            } else if (clasePadre.equals(Simbolo.ENTERO)) {
                return true;
            } else if (clasePadre.equals(Simbolo.DECIMAL)) {
                return true;
            } else if (clasePadre.equals(Simbolo.CADENA)) {
                return true;
            }

        } else if (claseHija.equals(Simbolo.DECIMAL)) {
            if (clasePadre.equals(Simbolo.CARACTER)) {
                return false;
            } else if (clasePadre.equals(Simbolo.ENTERO)) {
                return true;
            } else if (clasePadre.equals(Simbolo.DECIMAL)) {
                return true;
            } else if (clasePadre.equals(Simbolo.CADENA)) {
                return true;
            }

        } else if (claseHija.equals(Simbolo.CADENA)) {
            if (clasePadre.equals(Simbolo.CADENA)) {
                return true;
            }
        } else {
            MisSimbolos aux = Sintactico.clases.get(claseHija);

            while (aux != null) {
                if (aux.getClase().equals(clasePadre)) {
                    return true;
                }
                aux = aux.getPadre();
            }
        }
        return false;
    }

    public static String toString(Simbolo s, String contexto, int linea,
            int columna) {
        String cad1 = "<error>";

        if (s.getTipo() == Simbolo.DECIMAL || s.getTipo() == Simbolo.ENTERO) {
            String direccion = generaTemporal();
            set(direccion, s(), "+", contexto, "Direccion del primer param");

            String num = obtenerValor(s);

            set(stack(direccion), num, "Pasamos el numero");

            set(s(), s(), "+", contexto);
            call("__nString__");
            set(s(), s(), "-", contexto);

            direccion = generaTemporal();
            set(direccion, s(), "+", contexto, "Direccion del return");
            String retorno = generaTemporal();
            set(retorno, stack(direccion));

            cad1 = retorno;
        } else if (s.getTipo() == Simbolo.LOGICO) {
            String direccion = generaTemporal();
            // String param = generaTemporal();
            set(direccion, s(), "+", contexto, "Direccion del primer param");

            String bool = obtenerValor(s);

            set(stack(direccion), bool, "Pasamos el booleano");

            set(s(), s(), "+", contexto);
            call("__bString__");
            set(s(), s(), "-", contexto);

            direccion = generaTemporal();
            set(direccion, s(), "+", contexto, "Direccion del return");
            String retorno = generaTemporal();
            set(retorno, stack(direccion));

            cad1 = retorno;
        } else if (s.getTipo() == Simbolo.CARACTER) {
            cad1 = charToString(obtenerValor(s));
        } else if (s.getTipo() == Simbolo.CADENA) {
            cad1 = obtenerValor(s);
        } else {
            Errores.errorSemantico("No se puede convertir a cadena", linea,
                    columna);
        }

        return cad1;
    }

    public static Simbolo suma(Simbolo x, Simbolo y, String contexto,
            int linea, int columna) {
        Simbolo RESULT = new Simbolo();

        RESULT.setTipo(Simbolo.numerico(x, y));

        if (x.getTipo2() == Simbolo.ARRAY || y.getTipo2() == Simbolo.ARRAY) {
            Errores.errorSemantico("Operacion no permitida.", linea, columna);
            return new Simbolo();
        }

        if (RESULT.getTipo() == Simbolo.VACIO) {
            if ((x.getTipo() == Simbolo.CADENA && castImplicito(y.getTipo(),
                    Simbolo.CADENA))
                    || (y.getTipo() == Simbolo.CADENA && castImplicito(
                            x.getTipo(), Simbolo.CADENA))) {
                String cad1 = toString(x, contexto, linea, columna);
                String cad2 = toString(y, contexto, linea, columna);

                String desplazamiento = generaTemporal();

                set(desplazamiento, s(), "+", contexto);
                set(stack(desplazamiento), cad1);
                String desplazamiento2 = generaTemporal();
                set(desplazamiento2, desplazamiento, "+", "1");
                set(stack(desplazamiento2), cad2);

                set(s(), s(), "+", contexto);
                call("__concat__");
                set(s(), s(), "-", contexto);

                String direccion = generaTemporal();
                set(direccion, s(), "+", contexto, "Direccion del return");

                // String retorno = generaTemporal();
                // set(retorno, stack(direccion));
                RESULT.setId(direccion);
                RESULT.setTipo(Simbolo.CADENA);
                RESULT.setLugar(Simbolo.HEAP);
                RESULT.setTipo2(Simbolo.LITERAL);
            } else {
                Errores.errorSemantico("Operacion no permitida.", linea,
                        columna);
                return RESULT;
            }
        }
        if (RESULT.getTipo() != Simbolo.CADENA) {
            if (RESULT.getTipo() == Simbolo.VACIO) {
                Errores.errorSemantico("Tipos incompatibles.", linea, columna);
            } else {
                String a = obtenerValor(x);
                String b = obtenerValor(y);
                String r = generaTemporal();
                set(r, a, "+", b, "Suma aritmetica");
                RESULT.setId(r);
            }
        }
        RESULT.setTipo2(Simbolo.VALOR);

        return RESULT;
    }

    public static Simbolo aritmetica(Simbolo x, Simbolo y, String operador,
            int linea, int columna) {
        Simbolo RESULT = new Simbolo();
        RESULT.setTipo(Simbolo.numerico(x, y));
        if (RESULT.getTipo() == Simbolo.VACIO || x.getTipo() == Simbolo.CADENA
                || y.getTipo() == Simbolo.CADENA) {
            Errores.errorSemantico("No son tipos numericos.", linea, columna);
        }
        String a = obtenerValor(x);
        String b = obtenerValor(y);
        String r = generaTemporal();
        set(r, a, operador, b, "Operacion Aritmetica " + operador);
        RESULT.setId(r);
        RESULT.setTipo2(Simbolo.VALOR);
        return RESULT;
    }

    public static void comparar(Simbolo e1, String operadorRelacional,
            Simbolo e2, String etqF, String etqV, int linea, int columna) {
        if (e2 != null) {

            if (e1.getTipo() == Simbolo.CADENA
                    || e2.getTipo() == Simbolo.CADENA) {
                if (e1.getTipo() == Simbolo.CADENA
                        && e2.getTipo() == Simbolo.CADENA
                        && (operadorRelacional.equals("==") || operadorRelacional
                        .equals("!="))) {
                    String etqI = generaEtiqueta();
                    String cmp1 = "";
                    String cmp2 = "";
                    if (e1.getTipo2() != Simbolo.LITERAL) {
                        cmp1 = obtenerValor(e1);
                    } else {
                        cmp1 = e1.getId();
                    }

                    if (e2.getTipo2() != Simbolo.LITERAL) {
                        cmp2 = obtenerValor(e2);
                    } else {
                        cmp2 = e2.getId();
                    }

                    String __byte1__ = generaTemporal();
                    String __byte2__ = generaTemporal();
                    label(etqI, "Iniciamos la comparacion de cadenas");
                    set(__byte1__, heap(cmp1));
                    set(__byte2__, heap(cmp2));
                    if (operadorRelacional.equals("!=")) {
                        jumpIf(__byte1__, "!=", __byte2__, etqV);
                        jumpIf(__byte1__, "==", "0", etqF);
                    } else {
                        jumpIf(__byte1__, "!=", __byte2__, etqF);
                        jumpIf(__byte1__, "==", "0", etqV);
                    }
                    set(cmp1, cmp1, "+", "1");
                    set(cmp2, cmp2, "+", "1");
                    jump(etqI);
                } else {
                    Errores.errorSemantico("No se pueden comparar los tipos.",
                            linea, linea);
                }
            } else if (e1.getTipo() == Simbolo.LOGICO
                    || e2.getTipo() == Simbolo.LOGICO) {
                if (e1.getTipo() == Simbolo.LOGICO
                        && e2.getTipo() == Simbolo.LOGICO) {
                    String cmp1 = obtenerValor(e1);
                    String cmp2 = obtenerValor(e2);
                    jumpIf(cmp1, operadorRelacional, cmp2, etqV);
                    jump(etqF);
                } else {
                    Errores.errorSemantico("No se pueden comparar los tipos.",
                            linea, columna);
                }
            } else {
                String cmp1 = obtenerValor(e1);
                String cmp2 = obtenerValor(e2);
                jumpIf(cmp1, operadorRelacional, cmp2, etqV);
                jump(etqF);
            }
        } else {
            if (e1.getTipo() == Simbolo.CADENA) {
                Errores.errorSemantico("Condicion no valida.", linea, columna);
            } else {
                String cmp1 = obtenerValor(e1);
                jumpIf(cmp1, operadorRelacional, "0", etqV);
                jump(etqF);
            }
        }
    }

}
