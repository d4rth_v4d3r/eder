package org.compi2;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

public class Main extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1523263428698759270L;
	private JPanel contentPane;
	private JMenuBar menuBar;
	private JMenu mnArchivo;
	private JMenu mnCompilar;
	private JMenu mnErrores;
	private JMenuItem mntmAbrir;
	private JMenuItem mntmGuardar;
	private JMenuItem mntmCerrar;
	private JMenuItem mntmSalir;
	private JMenuItem mntmTablaDeSimbolos;
	private JMenuItem mntmGenerarCodigo;
	private JMenuItem mntmEjecutar;
	private JMenuItem mntmOptimizar;
	private JMenuItem mntmLexicos;
	private JMenuItem mntmSintacticos;
	private JMenuItem mntmSemanticos;
	private IDE gui;
	private JMenu mnNuevo;
	private JMenuItem mntmArchivo;
	private JMenuItem mntmProyecto;

	/**
	 * Launch the application.
	 * 
	 * @throws Exception
	 */
	public static void main(final String[] args) throws Exception {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					try {
						UIManager
								.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
					} catch (Exception e) {// Manejo de excepción...
						try {
							UIManager
									.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
						} catch (Exception e1) {// Manejo de excepción...
							e.printStackTrace();
						}
					}
					if (args != null && args.length > 0)
						Proyecto.dotPath = args[0];

					Errores.modoConsola = false;
					Main frame = new Main();
					frame.setVisible(true);
					frame.pack();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

	/**
	 * Create the frame.
	 */
	public Main() {
		setTitle("PROYECTO-COMPILADORES2");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 524, 389);

		this.menuBar = new JMenuBar();
		setJMenuBar(this.menuBar);

		this.mnArchivo = new JMenu("Archivo");
		this.menuBar.add(this.mnArchivo);

		this.mnNuevo = new JMenu("Nuevo");
		this.mnArchivo.add(this.mnNuevo);

		this.mntmArchivo = new JMenuItem("Archivo");
		this.mntmArchivo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				gui.nuevoArchivo();
			}
		});
		this.mnNuevo.add(this.mntmArchivo);

		this.mntmProyecto = new JMenuItem("Proyecto");
		this.mntmProyecto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				gui.nuevoProyecto();
			}
		});
		this.mnNuevo.add(this.mntmProyecto);

		this.mntmAbrir = new JMenuItem("Abrir");
		mntmAbrir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				gui.abrirProyecto();
			}
		});
		this.mnArchivo.add(this.mntmAbrir);

		this.mntmGuardar = new JMenuItem("Guardar");
		mntmGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				gui.guardarArchivo();
			}
		});
		this.mnArchivo.add(this.mntmGuardar);

		this.mntmCerrar = new JMenuItem("Cerrar");
		mntmCerrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				gui.cerrarArchivo();
			}
		});
		this.mnArchivo.add(this.mntmCerrar);

		this.mntmSalir = new JMenuItem("Salir");
		this.mntmSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		this.mnArchivo.add(this.mntmSalir);

		this.mnCompilar = new JMenu("Compilar");
		this.menuBar.add(this.mnCompilar);

		this.mntmTablaDeSimbolos = new JMenuItem("Tabla de Simbolos");
		mntmTablaDeSimbolos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				gui.compilar(true);
			}
		});
		this.mnCompilar.add(this.mntmTablaDeSimbolos);

		this.mntmGenerarCodigo = new JMenuItem("Generar Codigo");
		this.mntmGenerarCodigo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				gui.compilar(false);
			}
		});
		this.mnCompilar.add(this.mntmGenerarCodigo);

		this.mntmEjecutar = new JMenuItem("Ejecutar");
		this.mntmEjecutar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				gui.ejecutar();
			}
		});
		this.mnCompilar.add(this.mntmEjecutar);

		this.mntmOptimizar = new JMenuItem("Optimizar");
		this.mntmOptimizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				gui.optimizar();
			}
		});
		this.mnCompilar.add(this.mntmOptimizar);

		this.mnErrores = new JMenu("Errores");
		this.menuBar.add(this.mnErrores);

		this.mntmLexicos = new JMenuItem("Lexicos");
		mntmLexicos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				gui.mostrarErroresLexicos();
			}
		});
		this.mnErrores.add(this.mntmLexicos);

		this.mntmSintacticos = new JMenuItem("Sintacticos");
		mntmSintacticos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				gui.mostrarErroresSintacticos();
			}
		});
		this.mnErrores.add(this.mntmSintacticos);

		this.mntmSemanticos = new JMenuItem("Semanticos");
		mntmSemanticos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				gui.mostrarErroresSemanticos();
			}
		});
		this.mnErrores.add(this.mntmSemanticos);
		this.contentPane = new JPanel();
		this.gui = new IDE();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.contentPane.setLayout(new BorderLayout(0, 0));
		this.contentPane.add(gui);
		setContentPane(this.contentPane);
	}

}
