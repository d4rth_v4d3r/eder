package org.compi2;

import java.util.ArrayList;

public class Errores {
	private static ArrayList<String> lexicos;
	private static ArrayList<String> sintacticos;
	private static ArrayList<String> semanticos;
	private static ArrayList<String> informacion;
	public static boolean modoConsola = true;

	public static void inicializar() {
		lexicos = new ArrayList<>();
		sintacticos = new ArrayList<>();
		semanticos = new ArrayList<>();
		informacion = new ArrayList<>();
	}

	public static void errorLexico(String mensaje, int linea, int columna) {
		error(lexicos, mensaje, linea, columna);
	}

	public static void errorSintactico(String mensaje, int linea, int columna) {
		error(sintacticos, mensaje, linea, columna);
	}

	public static void errorSemantico(String mensaje, int linea, int columna) {
		error(semanticos, mensaje, linea, columna);
	}

	public static void informacion(String mensaje, int linea, int columna) {
		error(informacion, mensaje, linea, columna);
	}

	public static void excepcion(String mensaje) {
		System.err.println("Error fatal en " + Generador.nombreArchivo + " : "
				+ mensaje);
	}

	public static void debug(String mensaje) {
		System.out.println(Generador.nombreArchivo + " : " + mensaje);
	}

	private static void error(ArrayList<String> lista, String mensaje,
			int linea, int columna) {
		if (modoConsola)
			lista.add(String.format("%s | '%s' :: (%d, %d)%s",
					Generador.nombreArchivo, mensaje, linea, columna,
					System.getProperty("line.separator")));
		else
			lista.add(String
					.format("<tr><td> %s </td><td> %s </td><td> %d </td><td> %d </td></tr>%s",
							Generador.nombreArchivo, mensaje, linea, columna,
							System.getProperty("line.separator")));
	}

	public static ArrayList<String> getErroresLexicos() {
		return lexicos;
	}

	public static ArrayList<String> getErroresSintacticos() {
		return sintacticos;
	}

	public static ArrayList<String> getErroresSemanticos() {
		return semanticos;
	}

	public static ArrayList<String> getInformacion() {
		return informacion;
	}

	public static boolean mostrarErrores() {
		boolean tieneErrores = false;

		tieneErrores = tieneErrores
				|| mostrarErrores(lexicos, "ERRORES LEXICOS");
		tieneErrores = tieneErrores
				|| mostrarErrores(sintacticos, "ERRORES SINTACTICOS");
		tieneErrores = tieneErrores
				|| mostrarErrores(semanticos, "ERRORES SEMANTICOS");

		mostrarInformacion(informacion, "ADVERTENCIAS");

		return tieneErrores;
	}

	private static boolean mostrarErrores(ArrayList<String> lista, String titulo) {
		if (lista.size() > 0) {

			System.err.println(String.format(
					"---------------- %s ----------------", titulo));

			for (String error : lista) {
				System.err.println(error);
			}

			return true;
		}

		return false;
	}

	private static boolean mostrarInformacion(ArrayList<String> lista,
			String titulo) {
		if (lista.size() > 0) {

			System.out.println(String.format(
					"---------------- %s ----------------", titulo));

			for (String error : lista) {
				System.out.println(error);
			}

			return true;
		}

		return false;
	}

}
