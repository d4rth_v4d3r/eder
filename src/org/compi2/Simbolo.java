/**
 * 
 */
package org.compi2;

import java.util.ArrayList;

public class Simbolo {

	public static final String ENTERO = "int", CADENA = "string",
			LOGICO = "boolean", DECIMAL = "float", CARACTER = "char",
			VACIO = "void", HEAP = "heap", STACK = "stack", PUBLICO = "public",
			PRIVADO = "private", PROTEGIDO = "protected";

	public static final String table_cell = "|%-35s|%-35s|%-10s|%-10s|%-10s|%-35s|%-10s|%-10s|%-35s|";
	public static final String table_line = String.format(
			"+%-35s+%-35s+%-10s+%-10s+%-10s+%-35s+%-10s+%-10s+%-35s+", "", "",
			"", "", "", "", "", "", "").replaceAll(" ", "-");

	public static final String METODO = "METODO", VARIABLE = "VARIABLE",
			ARRAY = "ARREGLO", REFERENCIA = "REFERENCIA", VALOR = "VALOR",
			VLOCAL = "VLOCAL", VOBJ = "VGLOBAL", AOBJ = "AOBJ",
			ALOCAL = "ALOCAL", CONSTRUCTOR = "CONS", TEMPORAL = "TEMPORAL",
			LITERAL = "LITERAL";
	public static final String newline = System.getProperty("line.separator");

	private String id, tipo;
	private int tamano, pos;
	private Object extra;
	private String acceso;
	private String lugar;
	private String ambiente;
	private String tipo2;
	public boolean inicializado;
	public int linea, columna;

	/**
	 * 
	 */
	public Simbolo() {
		// TODO Auto-generated constructor stub
		setId("<id>");
		setTipo(VACIO);
		setTamano(0);
		setPos(0);
		putExtra(null);
		setAcceso(PRIVADO);
		setLugar(null);
		setAmbiente(null);
		setTipo2(VARIABLE);
		inicializado = false;
	}

	public Simbolo(String id, String tipo, int tamano, int pos, Object extra,
			String acceso, String lugar, String ambiente, String tipo2,
			boolean inicializado) {
		// TODO Auto-generated constructor stub
		setId(id);
		setTipo(tipo);
		setTamano(tamano);
		setPos(pos);
		putExtra(extra);
		setAcceso(acceso);
		setLugar(lugar);
		setAmbiente(ambiente);
		setTipo2(tipo2);
		this.inicializado = inicializado;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public int getTamano() {
		return tamano;
	}

	public void setTamano(int tamano) {
		this.tamano = tamano;
	}

	public int getPos() {
		return pos;
	}

	public void setPos(int pos) {
		this.pos = pos;
	}

	public Object getExtra() {
		return extra;
	}

	public void putExtra(Object extra) {
		this.extra = extra;
	}

	public String getAcceso() {
		return acceso;
	}

	public void setAcceso(String acceso) {
		this.acceso = acceso;
	}

	public String getLugar() {
		return lugar;
	}

	public void setLugar(String lugar) {
		this.lugar = lugar;
	}

	public String getAmbiente() {
		return ambiente;
	}

	public void setAmbiente(String ambiente) {
		this.ambiente = ambiente;
	}

	public String getTipo2() {
		return tipo2;
	}

	public void setTipo2(String tipo2) {
		this.tipo2 = tipo2;
	}

	public Simbolo duplicar() {
		return new Simbolo(id, tipo, tamano, pos, extra, acceso, lugar,
				ambiente, tipo2, inicializado);
	}

	public static String numerico(Simbolo x, Simbolo y) {
		if (x.tipo == ENTERO || x.tipo == DECIMAL)
			if (y == null)
				return x.tipo;
			else if (y.tipo == ENTERO)
				return ENTERO;
			else if (y.tipo == DECIMAL)
				return DECIMAL;
			else if (y.tipo == CARACTER)
				return ENTERO;
		if (x.tipo == DECIMAL)
			if (y == null)
				return x.tipo;
			else if (y.tipo == ENTERO)
				return DECIMAL;
			else if (y.tipo == DECIMAL)
				return DECIMAL;
			else if (y.tipo == CARACTER)
				return DECIMAL;

		return VACIO;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return String.valueOf(this.pos);
	}

	public String toH() {
		// TODO Auto-generated method stub
		Object extra = this.extra;

		if (this.tipo2 == METODO) {
			String firma = "";
			@SuppressWarnings("unchecked")
			ArrayList<Simbolo> params = (ArrayList<Simbolo>) this.getExtra();
			for (Simbolo s : params)
				firma += ((s.getTipo2() == REFERENCIA ? "&" : "") + s.getId() + "_");
			extra = firma;
		}

		return String.format(table_cell, this.id, this.tipo, this.acceso,
				this.tamano, this.pos, this.ambiente, this.tipo2, this.lugar,
				extra, inicializado);
	}

	public StringBuffer toHTML() {
		StringBuffer html = new StringBuffer();
		html.append("<TR>");
		html.append("<TD>" + this.id + "</TD>");
		html.append("<TD>" + this.tipo + "</TD>");
		html.append("<TD>" + this.acceso + "</TD>");
		html.append("<TD>" + this.tamano + "</TD>");
		html.append("<TD>" + this.pos + "</TD>");
		html.append("<TD>" + (this.lugar != STACK ? "*" + this.ambiente
				: this.ambiente) + "</TD>");
		html.append("<TD>" + this.tipo2 + "</TD>");
		html.append("<TD>" + (this.lugar == null ? "ninguno" : this.lugar)
				+ "</TD>");
		html.append("<TD>"
				+ (this.extra == null ? "ninguna" : this.extra.toString()
						.replaceAll(" ", "&nbsp;")) + "</TD>");
		html.append("</TR>");
		return html;
	}

}
