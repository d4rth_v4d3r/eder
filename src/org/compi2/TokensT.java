
//----------------------------------------------------
// The following code was generated by CUP v0.11a beta 20060608
// Wed Jul 09 23:40:56 CST 2014
//----------------------------------------------------

package org.compi2;

/** CUP generated interface containing symbol constants. */
public interface TokensT {
  /* terminals */
  public static final int HEREDA = 4;
  public static final int PUBLICO = 15;
  public static final int VALENTERO = 66;
  public static final int PORIGUAL = 59;
  public static final int POR = 26;
  public static final int SELECTOR = 41;
  public static final int IMPRIMIR = 23;
  public static final int SI = 39;
  public static final int ENTERO = 17;
  public static final int BRACKETI = 55;
  public static final int TERNARIO = 52;
  public static final int BRACKETD = 56;
  public static final int REF = 12;
  public static final int ENCASO = 42;
  public static final int NOT = 38;
  public static final int AND = 36;
  public static final int PRIVADO = 16;
  public static final int IGUAL = 34;
  public static final int MASIGUAL = 57;
  public static final int SINO = 40;
  public static final int ARCHIVO = 6;
  public static final int CARACTER = 19;
  public static final int OR = 37;
  public static final int MIENTRAS = 45;
  public static final int DIV = 27;
  public static final int INCREMENTO = 64;
  public static final int SALTARCICLO = 48;
  public static final int IMPORTAR = 5;
  public static final int UMENOS = 29;
  public static final int THIS = 62;
  public static final int EXP = 28;
  public static final int ID = 3;
  public static final int VALCADENA = 54;
  public static final int ARREGLO = 61;
  public static final int EOF = 0;
  public static final int DECIMAL = 18;
  public static final int VALLOGICO = 53;
  public static final int CLASE = 2;
  public static final int DECREMENTO = 65;
  public static final int error = 1;
  public static final int DIVISIONIGUAL = 60;
  public static final int COMA = 8;
  public static final int CADENA = 21;
  public static final int MENOS = 25;
  public static final int MENOR = 32;
  public static final int LOGICO = 20;
  public static final int PORDEFECTO = 44;
  public static final int NO_IGUAL = 35;
  public static final int MAYOR = 30;
  public static final int NUEVO = 9;
  public static final int PUNTO = 22;
  public static final int VACIO = 14;
  public static final int VALCARACTER = 67;
  public static final int MAYOR_IGUAL = 31;
  public static final int PARI = 11;
  public static final int NUMERAL = 49;
  public static final int MENOSIGUAL = 58;
  public static final int HACER = 63;
  public static final int DPUNTOS = 43;
  public static final int ASIGN = 7;
  public static final int PARD = 10;
  public static final int LLAVEI = 50;
  public static final int PARA = 46;
  public static final int RETORNAR = 13;
  public static final int MENOR_IGUAL = 33;
  public static final int LLAVED = 51;
  public static final int SALTO = 47;
  public static final int VALDECIMAL = 68;
  public static final int MAS = 24;
}

