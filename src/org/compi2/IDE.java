package org.compi2;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTree;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.UIManager;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import org.compi2.interprete.Interprete;
import org.compi2.optimizador.SintacticoBloques;
import org.compi2.optimizador.SintacticoGrafo;

public class IDE extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3047419709668989071L;
	private JPanel panel;
	private JTabbedPane tabbedPane;
	private JTree tree;
	private ArrayList<Proyecto> proyectos;
	private DefaultMutableTreeNode workspace;
	private JPanel panel_1;
	private JLabel lblPosicion;
	public int caret;
	private JScrollPane scrollPane;
	private JTextArea txtrConsola;
	private JLabel lblStatus;
	private JPanel panel_2;

	/**
	 * Create the panel.
	 */
	public IDE() {
		this.caret = 0;
		this.panel = new JPanel();
		this.proyectos = new ArrayList<Proyecto>();
		workspace = new DefaultMutableTreeNode("Proyectos");

		this.tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		this.tabbedPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);

		this.panel_1 = new JPanel();

		this.scrollPane = new JScrollPane();

		this.panel_2 = new JPanel();
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(
				Alignment.LEADING).addGroup(
				groupLayout
						.createSequentialGroup()
						.addContainerGap()
						.addComponent(this.panel, GroupLayout.PREFERRED_SIZE,
								137, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(
								groupLayout
										.createParallelGroup(Alignment.LEADING)
										.addComponent(this.panel_2,
												GroupLayout.DEFAULT_SIZE, 499,
												Short.MAX_VALUE)
										.addComponent(this.panel_1,
												GroupLayout.DEFAULT_SIZE, 499,
												Short.MAX_VALUE)
										.addComponent(this.scrollPane,
												Alignment.TRAILING,
												GroupLayout.DEFAULT_SIZE, 499,
												Short.MAX_VALUE)
										.addComponent(this.tabbedPane,
												GroupLayout.DEFAULT_SIZE, 499,
												Short.MAX_VALUE))
						.addContainerGap()));
		groupLayout
				.setVerticalGroup(groupLayout
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								groupLayout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.TRAILING)
														.addGroup(
																groupLayout
																		.createSequentialGroup()
																		.addComponent(
																				this.tabbedPane,
																				GroupLayout.DEFAULT_SIZE,
																				292,
																				Short.MAX_VALUE)
																		.addPreferredGap(
																				ComponentPlacement.RELATED)
																		.addComponent(
																				this.scrollPane,
																				GroupLayout.PREFERRED_SIZE,
																				110,
																				GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				ComponentPlacement.RELATED)
																		.addComponent(
																				this.panel_2,
																				GroupLayout.PREFERRED_SIZE,
																				20,
																				GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				ComponentPlacement.RELATED)
																		.addComponent(
																				this.panel_1,
																				GroupLayout.PREFERRED_SIZE,
																				GroupLayout.DEFAULT_SIZE,
																				GroupLayout.PREFERRED_SIZE))
														.addComponent(
																this.panel,
																GroupLayout.DEFAULT_SIZE,
																456,
																Short.MAX_VALUE))
										.addGap(0)));

		this.lblPosicion = new JLabel("Linea, Columna");
		this.panel_2.add(this.lblPosicion);
		this.lblPosicion.setFont(new Font("Consolas", Font.PLAIN, 11));

		this.lblStatus = new JLabel("Bienvenido :)");
		this.panel_1.add(this.lblStatus);
		this.lblStatus.setFont(new Font("Consolas", Font.BOLD, 11));

		this.txtrConsola = new JTextArea();
		this.txtrConsola.setFont(new Font("Consolas", Font.PLAIN, 13));
		this.txtrConsola.setEditable(false);
		this.scrollPane.setViewportView(this.txtrConsola);
		this.panel.setLayout(new BorderLayout(0, 0));

		this.tree = new JTree(workspace);
		this.tree.setCellRenderer(new MyTreeCellRenderer());
		this.tree.getSelectionModel().setSelectionMode(
				TreeSelectionModel.SINGLE_TREE_SELECTION);
		this.panel.add(this.tree);
		setLayout(groupLayout);

		MouseListener ml = new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				int selRow = tree.getRowForLocation(e.getX(), e.getY());
				TreePath seleccion = tree
						.getPathForLocation(e.getX(), e.getY());
				if (selRow != -1) {
					if (e.getClickCount() == 2) {
						DefaultMutableTreeNode nodo = (DefaultMutableTreeNode) seleccion
								.getLastPathComponent();
						if (nodo.getUserObject() != null
								&& nodo.getUserObject() instanceof JTextArea
								&& !((JTextArea) nodo.getUserObject())
										.isEnabled()) {
							tree.setSelectionPath(new TreePath(
									((DefaultMutableTreeNode) nodo.getParent())
											.getPath()));
							nuevoArchivo(nodo.toString(), true, nodo);
						} else if (nodo.getUserObject() != null
								&& nodo.getUserObject() instanceof JTextArea
								&& ((JTextArea) nodo.getUserObject())
										.isEnabled()) {
							for (Component comp : tabbedPane.getComponents()) {
								JScrollPane jsp = null;
								if (comp instanceof JScrollPane)
									jsp = (JScrollPane) comp;
								if (jsp != null)
									if (jsp.getViewport().getView() == ((JTextArea) nodo
											.getUserObject())) {
										tabbedPane.setSelectedComponent(jsp);
										break;
									}
							}
						}
					}
				}
			}
		};
		tree.addMouseListener(ml);

	}

	public void nuevoArchivo() {
		nuevoArchivo(null, false, null);
	}

	public void compilar(boolean mostrarTabla) {
		int i = this.tabbedPane.getSelectedIndex();
		if (i >= 0 && this.tabbedPane.getTabCount() > 0) {
			this.guardarArchivo();
			this.proyectos.get(i).compilar(this.tabbedPane.getTitleAt(i));
			if (Errores.mostrarErrores()) {
				JOptionPane.showMessageDialog(this,
						"El analisis termino con errores.", "Error",
						JOptionPane.ERROR_MESSAGE);
				lblStatus.setText("El archivo tiene errores...");
			} else {
				JOptionPane.showMessageDialog(this,
						"El analisis termino correctamente.", "Informacion",
						JOptionPane.INFORMATION_MESSAGE);
				if (!mostrarTabla) {
					String title = this.tabbedPane.getTitleAt(i);
					title = title.replaceAll(".frc", ".3dr");
					JTextArea nuevoDocumento = null;
					JScrollPane sp = null;
					for (int j = 0; j < tabbedPane.getTabCount(); j++) {
						if (tabbedPane.getTitleAt(j).equals(title)
								&& proyectos.get(j) == proyectos.get(i)) {
							sp = (JScrollPane) this.tabbedPane
									.getComponentAt(j);
							nuevoDocumento = (JTextArea) sp.getViewport()
									.getView();
							break;
						}
					}
					if (nuevoDocumento == null) {
						sp = new JScrollPane();
						nuevoDocumento = new JTextArea();
						nuevoDocumento.setFont(new Font("Consolas", Font.PLAIN,
								13));
						nuevoDocumento.setEditable(false);
						sp.setViewportView(nuevoDocumento);
						this.tabbedPane.addTab(title, sp);

						this.proyectos.add(this.proyectos.get(i));
					}
					this.tabbedPane.setSelectedComponent(sp);
					nuevoDocumento.setText(this.proyectos.get(i).leerArchivo(
							title));
					if (this.proyectos.get(i).mostrarWarnings())
						lblStatus.setText("Archivo " + title
								+ " generado, con advertencias: "
								+ this.proyectos.get(i).getWorkspace()
								+ System.getProperty("file.separator")
								+ "Warnings.html");
					else
						lblStatus.setText("Archivo " + title
								+ " generado exitosamente.");
				} else {
					lblStatus.setText("Tabla de simbolos para "
							+ this.tabbedPane.getTitleAt(i)
							+ " generada exitosamente.");
				}
			}
			if (mostrarTabla)
				this.proyectos.get(i).mostrarTabla();
		}
	}

	public void ejecutar() {
		int i = this.tabbedPane.getSelectedIndex();
		if (i >= 0 && this.tabbedPane.getTabCount() > 0) {
			final String title = this.tabbedPane.getTitleAt(i);
			if (title.endsWith(".3dr")) {
				try {
					Interprete.consola = this.txtrConsola;
					final Interprete it = new Interprete(new File(
							this.proyectos.get(i).getWorkspace()
									+ System.getProperty("file.separator")
									+ title));
					it.run();
					it.dump();
					lblStatus.setText("Ejecucion terminada...");

					final int index = i;
					final JDialog jd = new JDialog() {

					};
					final JPanel contentPane = new JPanel() {
						@Override
						protected void paintComponent(Graphics arg0) {
							// TODO Auto-generated method stub
							super.paintComponent(arg0);
							arg0.drawImage(it.image, 0, 0, it.image.getWidth(),
									it.image.getHeight(), null);
						}

					};
					contentPane.setPreferredSize(new Dimension(it.width,
							it.height));
					jd.setContentPane(contentPane);
					jd.setTitle("GRAFICO GENERADO :)");
					jd.pack();
					jd.setVisible(true);
					File outputfile = new File(proyectos.get(index)
							.getWorkspace()
							+ System.getProperty("file.separator")
							+ title
							+ ".png");
					try {
						ImageIO.write(it.image, "png", outputfile);
						// Desktop.getDesktop().open(outputfile);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
						JOptionPane.showMessageDialog(null,
								"No se pudo generar el archivo...", "Error",
								JOptionPane.ERROR_MESSAGE);
					}

					if (it.errores.length() > 0)
						JOptionPane.showMessageDialog(this,
								it.errores.toString(), "Errores de ejecucion",
								JOptionPane.ERROR_MESSAGE);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(this,
							"No se pudo abrir el archivo " + title, "Error",
							JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
					lblStatus.setText("Error al abrir el archivo...");
				}

			} else {
				JOptionPane.showMessageDialog(this,
						"El archivo no es ejecutable " + title, "Error",
						JOptionPane.ERROR_MESSAGE);
				lblStatus.setText("El archivo parece no ser 3dr...");
			}

		}
	}

	public void optimizar() {
		int i = this.tabbedPane.getSelectedIndex();
		if (i >= 0 && this.tabbedPane.getTabCount() > 0) {
			final String title = this.tabbedPane.getTitleAt(i);
			if (title.endsWith(".3dr")) {
				try {
					Sintactico.workspace = this.proyectos.get(i).getWorkspace()
							+ System.getProperty("file.separator");
					SintacticoGrafo.Analisis(title, "rosybrown1");
					this.proyectos.get(i).generarDOT(title + ".dot");
					SintacticoBloques.Analisis(title);
					this.proyectos.get(i).guardarArchivo(
							title.replaceAll(".3dr", ".opt.3dr"),
							SintacticoBloques.getCodBLoque());
					SintacticoGrafo.Analisis(
							title.replaceAll(".3dr", ".opt.3dr"),
							"darkseagreen1");
					this.proyectos.get(i).generarDOT(
							title.replaceAll(".3dr", ".opt.3dr") + ".dot");

					JTextArea nuevoDocumento = null;
					JScrollPane sp = null;
					for (int j = 0; j < tabbedPane.getTabCount(); j++) {
						if (tabbedPane.getTitleAt(j).equals(
								title.replaceAll(".3dr", ".opt.3dr"))
								&& proyectos.get(j) == proyectos.get(i)) {
							sp = (JScrollPane) this.tabbedPane
									.getComponentAt(j);
							nuevoDocumento = (JTextArea) sp.getViewport()
									.getView();
							break;
						}
					}
					if (nuevoDocumento == null) {
						sp = new JScrollPane();
						nuevoDocumento = new JTextArea();
						nuevoDocumento.setFont(new Font("Consolas", Font.PLAIN,
								13));
						nuevoDocumento.setEditable(false);
						sp.setViewportView(nuevoDocumento);
						this.tabbedPane.addTab(
								title.replaceAll(".3dr", ".opt.3dr"), sp);

						this.proyectos.add(this.proyectos.get(i));
					}
					this.tabbedPane.setSelectedComponent(sp);
					nuevoDocumento.setText(this.proyectos.get(i).leerArchivo(
							title.replaceAll(".3dr", ".opt.3dr")));
					lblStatus.setText("Archivo " + title
							+ " generado exitosamente, generando grafos....");

					Thread.sleep(5000);
					Desktop.getDesktop()
							.open(new File(Sintactico.workspace + title
									+ ".dot.jpg"));
					Desktop.getDesktop().open(
							new File(Sintactico.workspace
									+ title.replaceAll(".3dr", ".opt.3dr")
									+ ".dot.jpg"));
				} catch (Exception e) {
					JOptionPane.showMessageDialog(this,
							"No se pudo abrir el archivo " + title, "Error",
							JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
					lblStatus.setText("Error al abrir el archivo...");
				}

			} else {
				JOptionPane.showMessageDialog(this,
						"El archivo no es de 3 direcciones " + title, "Error",
						JOptionPane.ERROR_MESSAGE);
				lblStatus.setText("El archivo parece no ser 3dr...");
			}

		}
	}

	public void mostrarErroresLexicos() {
		int i = this.tabbedPane.getSelectedIndex();
		if (i >= 0 && this.tabbedPane.getTabCount() > 0) {
			this.proyectos.get(i).compilar(this.tabbedPane.getTitleAt(i));
			if (!this.proyectos.get(i).mostrarErroresLexicos())
				JOptionPane.showMessageDialog(this, "No hay errores lexicos.",
						"Informacion", JOptionPane.INFORMATION_MESSAGE);
		}
	}

	public void mostrarErroresSintacticos() {
		int i = this.tabbedPane.getSelectedIndex();
		if (i >= 0 && this.tabbedPane.getTabCount() > 0) {
			this.proyectos.get(i).compilar(this.tabbedPane.getTitleAt(i));
			if (!this.proyectos.get(i).mostrarErroresSintacticos())
				JOptionPane.showMessageDialog(this,
						"No hay errores sintacticos.", "Informacion",
						JOptionPane.INFORMATION_MESSAGE);
		}
	}

	public void mostrarErroresSemanticos() {
		int i = this.tabbedPane.getSelectedIndex();
		if (i >= 0 && this.tabbedPane.getTabCount() > 0) {
			this.proyectos.get(i).compilar(this.tabbedPane.getTitleAt(i));
			if (!this.proyectos.get(i).mostrarErroresSemanticos())
				JOptionPane.showMessageDialog(this,
						"No hay errores semanticos.", "Informacion",
						JOptionPane.INFORMATION_MESSAGE);
		}
	}

	public void imprimir(String texto) {
		this.txtrConsola.append(texto);
	}

	public void nuevoProyecto() {
		File rootDir = mostrarDialogoGuardar(false, true);
		if (rootDir != null && rootDir.isDirectory()) {
			String nombre = JOptionPane.showInputDialog(this,
					"Ingrese el nombre del proyecto:", "Nuevo Proyecto",
					JOptionPane.QUESTION_MESSAGE);
			if (nombre != null && nombre.trim().length() > 0) {
				String path = rootDir.getAbsolutePath()
						+ System.getProperty("file.separator") + nombre;
				final Proyecto proy = new Proyecto(path, nombre);
				if (!proy.crear())
					JOptionPane
							.showMessageDialog(
									this,
									"No se puede crear el proyecto, ya existe una carpeta con el mismo nombre.",
									"Error", JOptionPane.ERROR_MESSAGE);

				DefaultMutableTreeNode nuevo = new DefaultMutableTreeNode(proy) {
					private static final long serialVersionUID = 4880991557555250134L;

					@Override
					public String toString() {
						// TODO Auto-generated method stub
						return proy.getNombre();
					}
				};
				((DefaultTreeModel) tree.getModel()).insertNodeInto(nuevo,
						workspace, workspace.getChildCount());
				expandAllNodes(tree, 0, tree.getRowCount());
				lblStatus.setText("Proyecto " + proy.getNombre() + " creado.");
			}
		}
	}

	public void abrirProyecto() {
		File f = mostrarDialogoGuardar(true, false);

		if (f != null) {
			final Proyecto p = Proyecto.abrirProyecto(f.getAbsolutePath());
			if (p != null) {
				DefaultMutableTreeNode nuevo = new DefaultMutableTreeNode(p) {
					private static final long serialVersionUID = 4880991557555250134L;

					@Override
					public String toString() {
						// TODO Auto-generated method stub
						return p.getNombre();
					}
				};
				((DefaultTreeModel) tree.getModel()).insertNodeInto(nuevo,
						workspace, workspace.getChildCount());
				expandAllNodes(tree, 0, tree.getRowCount());
				tree.setSelectionPath(new TreePath(nuevo.getPath()));
				DefaultMutableTreeNode archivoPrincipal = null;
				int index = 0;

				for (String archivo : p.getArchivos()) {
					DefaultMutableTreeNode n = this.nuevoArchivo(archivo, true,
							null);
					if (p.isPrincipal(archivo)) {
						archivoPrincipal = n;
						index = tabbedPane.getTabCount() - 1;
					}
				}

				if (archivoPrincipal != null) {
					tree.setSelectionPath(new TreePath(archivoPrincipal
							.getPath()));
					tabbedPane.setSelectedIndex(index);
				}
				lblStatus.setText("Proyecto " + p.getNombre() + " abierto.");
			} else {
				JOptionPane.showMessageDialog(this, SintacticoP.mensaje,
						"Error", JOptionPane.ERROR_MESSAGE);
			}

		}
	}

	public void guardarArchivo() {
		int i = this.tabbedPane.getSelectedIndex();
		if (i >= 0 && this.tabbedPane.getTabCount() > 0) {
			this.proyectos.get(i).guardarArchivo(
					this.tabbedPane.getTitleAt(i),
					((JTextArea) ((JScrollPane) this.tabbedPane
							.getSelectedComponent()).getViewport().getView())
							.getText());
			lblStatus.setText("Archivo " + this.tabbedPane.getTitleAt(i)
					+ " guardado.");
		}
	}

	public void cerrarArchivo() {
		int i = this.tabbedPane.getSelectedIndex();
		if (i >= 0 && this.tabbedPane.getTabCount() > 0) {
			((JTextArea) ((JScrollPane) this.tabbedPane.getComponentAt(i))
					.getViewport().getView()).setEnabled(false);
			lblStatus.setText("Archivo " + this.tabbedPane.getTitleAt(i)
					+ " cerrado.");
			this.tabbedPane.removeTabAt(i);
			this.proyectos.remove(i);
		}
	}

	private File mostrarDialogoGuardar(boolean archivo, boolean guardar) {
		JFileChooser jf = new JFileChooser();

		jf.setMultiSelectionEnabled(false);
		if (archivo)
			jf.setFileSelectionMode(JFileChooser.FILES_ONLY);
		else
			jf.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

		int res = 0;
		if (guardar)
			res = jf.showSaveDialog(this);
		else {
			FileNameExtensionFilter filter = new FileNameExtensionFilter(
					"Proyectos", "pj");
			jf.setFileFilter(filter);
			res = jf.showOpenDialog(this);
		}

		switch (res) {
		case JFileChooser.APPROVE_OPTION:
			return jf.getSelectedFile();

		default:
			return null;
		}

	}

	private DefaultMutableTreeNode nuevoArchivo(String title, boolean abrir,
			DefaultMutableTreeNode display) {

		TreePath seleccion = (TreePath) tree.getSelectionPath();

		if (seleccion != null) {
			DefaultMutableTreeNode nodo = (DefaultMutableTreeNode) seleccion
					.getLastPathComponent();

			if (nodo.getUserObject() instanceof Proyecto) {
				Proyecto proy = (Proyecto) nodo.getUserObject();
				if (title == null) {
					String nombre = JOptionPane.showInputDialog(this,
							"Ingrese el nombre del archivo:", "Nuevo Archivo",
							JOptionPane.QUESTION_MESSAGE);
					if (nombre != null && nombre.trim().length() > 0) {
						if (!nombre.endsWith(".frc"))
							nombre += ".frc";

						title = nombre;

						try {
							if (!proy.agregarArchivo(title, true))
								JOptionPane.showMessageDialog(this,
										"No se pudo agregar el archivo.",
										"Error", JOptionPane.ERROR_MESSAGE);
						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}
					} else
						return null;
				}

				JScrollPane sp = new JScrollPane();
				JTextArea nuevoDocumento = new JTextArea();
				nuevoDocumento.setFont(new Font("Consolas", Font.PLAIN, 13));

				// El siguiente codigo fue tomado de:
				// http://stackoverflow.com/questions/5139995/java-column-number-and-line-number-of-cursors-current-position
				// Add a caretListener to the editor. This is an anonymous class
				// because it is inline and has no specific name.
				nuevoDocumento.addCaretListener(new CaretListener() {
					// Each time the caret is moved, it will trigger the
					// listener and its method caretUpdate.
					// It will then pass the event to the update method
					// including the source of the event (which is our textarea
					// control)
					public void caretUpdate(CaretEvent e) {
						JTextArea editArea = (JTextArea) e.getSource();

						// Lets start with some default values for the line and
						// column.
						int linenum = 1;
						int columnnum = 1;

						// We create a try catch to catch any exceptions. We
						// will simply ignore such an error for our
						// demonstration.
						try {
							// First we find the position of the caret. This is
							// the number of where the caret is in relation to
							// the start of the JTextArea
							// in the upper left corner. We use this position to
							// find offset values (eg what line we are on for
							// the given position as well as
							// what position that line starts on.
							int caretpos = editArea.getCaretPosition();
							caret = caretpos;
							linenum = editArea.getLineOfOffset(caretpos);

							// We subtract the offset of where our line starts
							// from the overall caret position.
							// So lets say that we are on line 5 and that line
							// starts at caret position 100, if our caret
							// position is currently 106
							// we know that we must be on column 6 of line 5.
							columnnum = caretpos
									- editArea.getLineStartOffset(linenum);

							// We have to add one here because line numbers
							// start at 0 for getLineOfOffset and we want it to
							// start at 1 for display.
							linenum += 1;
						} catch (Exception ex) {
						}

						// Once we know the position of the line and the column,
						// pass it to a helper function for updating the status
						// bar.
						lblPosicion.setText(String.format(
								"Linea: %d, Columna: %d", linenum,
								columnnum + 1));
					}
				});
				sp.setViewportView(nuevoDocumento);
				this.proyectos.add(proy);
				this.tabbedPane.addTab(title, sp);
				final String nombre = title;
				DefaultMutableTreeNode nuevo = null;
				if (display == null) {
					nuevo = new DefaultMutableTreeNode(nuevoDocumento) {
						private static final long serialVersionUID = 4880991557555250134L;

						@Override
						public String toString() {
							// TODO Auto-generated method stub
							return nombre;
						}
					};
					((DefaultTreeModel) tree.getModel()).insertNodeInto(nuevo,
							nodo, nodo.getChildCount());
				} else {
					nuevo = display;
					nuevo.setUserObject(nuevoDocumento);
				}

				expandAllNodes(tree, 0, tree.getRowCount());
				if (abrir)
					try {
						nuevoDocumento.setText(proy.leerArchivo(title));
					} catch (Exception e) {
						JOptionPane.showMessageDialog(this, e.getMessage(),
								"Error", JOptionPane.ERROR_MESSAGE);
					}
				lblStatus.setText("Archivo " + title + " abierto.");
				return nuevo;
			} else {
				JOptionPane.showMessageDialog(this, "Selecciona un proyecto.",
						"Advertencia", JOptionPane.WARNING_MESSAGE);
			}
		} else {
			JOptionPane.showMessageDialog(this, "Selecciona un proyecto.",
					"Advertencia", JOptionPane.WARNING_MESSAGE);
		}

		return null;
	}

	private void expandAllNodes(JTree tree, int startingIndex, int rowCount) {
		for (int i = startingIndex; i < rowCount; ++i) {
			tree.expandRow(i);
		}

		if (tree.getRowCount() != rowCount) {
			expandAllNodes(tree, rowCount, tree.getRowCount());
		}
	}

	// this is what you want
	private static class MyTreeCellRenderer extends DefaultTreeCellRenderer {

		/**
		 * 
		 */
		private static final long serialVersionUID = 5888176613581652903L;

		@Override
		public Component getTreeCellRendererComponent(JTree tree, Object value,
				boolean sel, boolean expanded, boolean leaf, int row,
				boolean hasFocus) {
			super.getTreeCellRendererComponent(tree, value, sel, expanded,
					leaf, row, hasFocus);

			// decide what icons you want by examining the node
			if (value instanceof DefaultMutableTreeNode) {
				DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
				if (node.getUserObject() instanceof JTextArea) {
					// your root node, since you just put a String as a user obj
					setIcon(UIManager.getIcon("Tree.leafIcon"));
				} else {
					//
					if (expanded)
						setIcon(UIManager.getIcon("Tree.openIcon"));
					else
						setIcon(UIManager.getIcon("Tree.closedIcon"));
				}
			}

			return this;
		}

	}
}
