package org.compi2.optimizador;

import java.util.LinkedList;

public class Arbol {
	private LinkedList<Arbol> arbol = new LinkedList<>();
	private String valor;
	private String produccion;
	private char tipo;

	public Arbol() {
	}

	public Arbol(String valor, String produccion, char tipo) {
		this.produccion = produccion;
		this.valor = valor;
		this.tipo = tipo;
	}

	public void AgregarNodo(String valor, String produccion, char tipo) {
		arbol.add(new Arbol(valor, produccion, tipo));
	}

	public void AgregarNodo(Arbol nuevo, String valor, String produccion,
			char tipo) {
		nuevo.produccion = produccion;
		nuevo.valor = valor;
		nuevo.tipo = tipo;
		arbol.add(nuevo);
	}

	public String getValor() {
		return this.valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public String getProduccion() {
		return this.produccion;
	}

	public char getTipo() {
		return this.tipo;
	}

	public Arbol getNodo(int indice) {
		return arbol.get(indice);
	}

	public LinkedList<Arbol> getArbol() {
		return arbol;
	}

	public int getCantidadNodos() {
		return arbol.size();
	}

}
