/**
 *
 */
package org.compi2;

import java.util.Hashtable;

public class MisSimbolos extends Hashtable<String, Simbolo> {

    /**
     *
     */
    private static final long serialVersionUID = -3972527126714604382L;
    public static final String plain_line = String.format(
            "+%-" + (54 + Simbolo.table_line.length()) + "s+", "").replaceAll(
                    " ", "-");
    private String clase;
    private MisSimbolos padre;
    private int tamano;
    private static String html_header = "<TABLE class=\"sample\">" + "<TR>"
            + "<TH> Id </TH>" + "<TH> Tipo </TH>" + "<TH> Acceso </TH>"
            + "<TH> Tamano </TH>" + "<TH> Posicion </TH>"
            + "<TH> Ambiente </TH>" + "<TH> Rol </TH>" + "<TH> Lugar </TH>"
            + "<TH> Informacion Extra </TH>" + "</TR>";
    public String codigoInit;

    public MisSimbolos(String clase, MisSimbolos padre) {
        this.setClase(clase);
        this.setPadre(padre);
        this.setTamano(0);
    }

    public String getCodigo() {
        MisSimbolos aux = this;
        StringBuilder codigo = new StringBuilder();

        while (aux != null) {
            codigo.append(Generador.getCodigo());
            aux = aux.padre;
        }
        return codigo.toString();
    }

    /**
     * Busca un simbolo en la tabla de simbolos y lo devuelve, considerando su
     * acceso. Esta busqueda se da en un árbol de herencia.
     *
     * @param key La clave del simbolo, compuesta por id mas el prefijo 'met_' o
     * 'var_'.
     * @param contexto El ambiente donde se encuentra la llamada al atributo.
     * @return El simbolo si es accesible o nulo en cualquier caso.
     */
    public Simbolo buscarSimbolo(String contexto, String key, int linea,
            int columna) {
        MisSimbolos aux = this;
        boolean protegido = aux.clase.equals(contexto);

        while (aux != null) {
            Simbolo s = aux.get(key);

            if (s != null) {
                if (s.getAcceso() == Simbolo.PRIVADO
                        && aux.clase.equals(contexto)) {
                    return s;
                } else if (!protegido && s.getAcceso() != Simbolo.PUBLICO) {
                    Errores.errorSemantico(
                            (key.startsWith("var") ? "La variable "
                            : "El metodo ")
                            + key.substring(key.lastIndexOf("_") + 1,
                                    key.length()) + " no es visible.",
                            linea, columna);
                    return null;
                } else {
                    return s;
                }
            } else {
                aux = aux.padre;
            }
        }
        return null;
    }

    /**
     * Busca un simbolo en la tabla de simbolos y lo devuelve, considerando su
     * acceso. Esta busqueda se da en un árbol de herencia.
     *
     * @param key_id La clave del simbolo, compuesta por id mas el prefijo
     * 'met_' o 'var_'.
     * @param contexto El ambiente donde se encuentra la llamada al atributo.
     * @return El simbolo si es accesible o nulo en cualquier caso.
     */
    public Simbolo buscarMetodo(String contexto, String key_id, int linea,
            int columna) {
        MisSimbolos aux = this;
        boolean protegido = aux.clase.equals(contexto);

        while (aux != null) {
            Simbolo s = null;

            outer:
            for (String key : aux.keySet()) {
                if (key_id.equals(key)) {
                    s = aux.get(key);
                    break outer;
                }

                String keyPreffix = key_id.replaceAll("met_([A-Za-z0-9]+)_.+",
                        "met_$1_");
                if (key.startsWith(keyPreffix)) {
                    String callParams[] = key_id.substring(keyPreffix.length(),
                            key_id.length()).split("_");
                    String metParams[] = key.substring(keyPreffix.length(),
                            key.length()).split("_");

                    if (callParams.length == metParams.length) {
                        for (int i = 0; i < metParams.length; i++) {
                            if (!Generador.castImplicito(callParams[i],
                                    metParams[i])) {
                                continue outer;
                            }
                        }

                        s = aux.get(key);
                        break outer;
                    } else {
                        continue outer;
                    }
                }
            }

            if (s != null) {
                if (s.getAcceso() == Simbolo.PRIVADO
                        && aux.clase.equals(contexto)) {
                    return s;
                } else if (!protegido && s.getAcceso() != Simbolo.PUBLICO) {
                    Errores.errorSemantico(
                            "El metodo "
                            + key_id.substring(
                                    key_id.lastIndexOf("_") + 1,
                                    key_id.length())
                            + " no es visible.", linea, columna);
                    return null;
                } else {
                    return s;
                }
            } else {
                aux = aux.padre;
            }
        }
        return null;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public String getClase() {
        return clase;
    }

    public void setClase(String clase) {
        this.clase = clase;
    }

    public MisSimbolos getPadre() {
        return padre;
    }

    public void setPadre(MisSimbolos padre) {
        this.padre = padre;
    }

    public int getTamano() {
        return tamano;
    }

    public void setTamano(int tamano) {
        this.tamano = tamano;
    }

    @Override
    public synchronized String toString() {
        // TODO Auto-generated method stub
        StringBuffer sb = new StringBuffer();
        String line = String.format("+%-55s", "").replace(" ", "-")
                + Simbolo.table_line;
        String tr = String.format("|%-55s", "KEY") + Simbolo.table_cell;
        String br = Simbolo.newline;

        sb.append(line + br);
        sb.append(String.format(tr, "ID", "TIPO", "ACCESO", "TAMANO", "POS",
                "AMBIENTE", "TIPO_S", "LUGAR", "INFO") + br);
        sb.append(line + br);

        for (String key : keySet()) {
            sb.append(String.format("|%-55s", key) + get(key).toString() + br);
        }

        sb.append(line + br);

        return sb.toString();
    }

    public StringBuffer toHTML() {
        StringBuffer html = new StringBuffer();
        html.append(html_header);
        for (String key : keySet()) {
            html.append(this.get(key).toHTML());
        }
        html.append("</TABLE>");
        return html;
    }
}
