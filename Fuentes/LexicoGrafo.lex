package org.compi2.optimizador;

import java_cup.runtime.Symbol;

class nodoC
{
    public Arbol nodo;
    public String cad = "";
    public nodoC(Arbol nodo)
    {
        this.nodo = nodo;
    }

    public nodoC() {
        
    }
}



%%

%{
 	public int getCantLineas(){
	  return yyline;
	}	
%}

%eofval{	
		return new Symbol(SimbolosGrafoNoOptimizado.EOF,yycolumn+1,yyline,yytext());
%eofval}

%cup 
%line
%char
%class LexicoGrafo
%public
%ignorecase

nument = "-"?[0-9]+("."[0-9]+)?
letra  = [a-zA-Z]
identificador = ({letra}|"$"|"_")({letra}|{nument}|"_"|"$")*
comentario = "//"([^\n])*

%%
"\"%c\""		   { return new Symbol(SimbolosGrafoNoOptimizado.numero,yycolumn+1,yyline,yytext()); }
"\"%f\""		   { return new Symbol(SimbolosGrafoNoOptimizado.numero,yycolumn+1,yyline,yytext()); }
"\"%d\""           { return new Symbol(SimbolosGrafoNoOptimizado.numero,yycolumn+1,yyline,yytext()); }
"if"               { return new Symbol(SimbolosGrafoNoOptimizado.dif, yycolumn+1,yyline, yytext());}
"if_not"           { return new Symbol(SimbolosGrafoNoOptimizado.difnot, yycolumn+1,yyline, yytext());}
"goto"             { return new Symbol(SimbolosGrafoNoOptimizado.jump, yycolumn+1,yyline, yytext());}
"int"              { return new Symbol(SimbolosGrafoNoOptimizado.dec, yycolumn+1,yyline, yytext());}
"method"           { return new Symbol(SimbolosGrafoNoOptimizado.proc, yycolumn+1,yyline, yytext());}
"{"                { return new Symbol(SimbolosGrafoNoOptimizado.begin, yycolumn+1,yyline, yytext());}
"}"                { return new Symbol(SimbolosGrafoNoOptimizado.end, yycolumn+1,yyline, yytext());}
"call"             { return new Symbol(SimbolosGrafoNoOptimizado.call, yycolumn+1,yyline, yytext());}
"print"            { return new Symbol(SimbolosGrafoNoOptimizado.print, yycolumn+1,yyline, yytext());}
"fprint"           { return new Symbol(SimbolosGrafoNoOptimizado.fprint, yycolumn+1,yyline, yytext());}
"float"           { return new Symbol(SimbolosGrafoNoOptimizado.float_, yycolumn+1,yyline, yytext());}
"-"                { return new Symbol(SimbolosGrafoNoOptimizado.menos, yycolumn+1,yyline, yytext()); }
"+"                { return new Symbol(SimbolosGrafoNoOptimizado.mas, yycolumn+1,yyline,yytext()); }
"*"                { return new Symbol(SimbolosGrafoNoOptimizado.por, yycolumn+1,yyline, yytext()); }
"/"                { return new Symbol(SimbolosGrafoNoOptimizado.div, yycolumn+1,yyline, yytext()); }
"%"                { return new Symbol(SimbolosGrafoNoOptimizado.mod, yycolumn+1,yyline, yytext()); }
"="                { return new Symbol(SimbolosGrafoNoOptimizado.asig, yycolumn+1,yyline, yytext()); }
"<"                { return new Symbol(SimbolosGrafoNoOptimizado.men, yycolumn+1,yyline, yytext()); }
">"                { return new Symbol(SimbolosGrafoNoOptimizado.may, yycolumn+1,yyline, yytext()); }
"<="               { return new Symbol(SimbolosGrafoNoOptimizado.meni, yycolumn+1,yyline, yytext()); }
">="               { return new Symbol(SimbolosGrafoNoOptimizado.mayi, yycolumn+1,yyline, yytext()); }
"=="               { return new Symbol(SimbolosGrafoNoOptimizado.igual, yycolumn+1,yyline, yytext()); }
"!="               { return new Symbol(SimbolosGrafoNoOptimizado.dife, yycolumn+1,yyline, yytext()); }
"("                { return new Symbol(SimbolosGrafoNoOptimizado.parena, yycolumn+1,yyline, yytext()); }
")"                { return new Symbol(SimbolosGrafoNoOptimizado.parenc, yycolumn+1,yyline, yytext()); }
"["                { return new Symbol(SimbolosGrafoNoOptimizado.corcha, yycolumn+1,yyline, yytext()); }
"]"                { return new Symbol(SimbolosGrafoNoOptimizado.corchc, yycolumn+1,yyline, yytext()); }
":"                { return new Symbol(SimbolosGrafoNoOptimizado.dospuntos, yycolumn+1,yyline, yytext());}
";"                { return new Symbol(SimbolosGrafoNoOptimizado.pcoma, yycolumn+1,yyline, yytext()); }
"."                { return new Symbol(SimbolosGrafoNoOptimizado.punto, yycolumn+1, yyline, yytext()); }
","                { return new Symbol(SimbolosGrafoNoOptimizado.coma, yycolumn+1, yyline, yytext());}
{nument}           { return new Symbol(SimbolosGrafoNoOptimizado.numero, yycolumn+1, yyline, yytext());}
{identificador}    { return new Symbol(SimbolosGrafoNoOptimizado.id, yycolumn+1,yyline, yytext());}
{comentario}       { }
[ \t\r\f\n]+       { }
.                  {System.err.println("Error Lexico "+yytext()+" linea: "+String.valueOf(yyline+1)+" columna "+String.valueOf(yycolumn+1+1)); }


