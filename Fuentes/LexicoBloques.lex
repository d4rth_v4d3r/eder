package org.compi2.optimizador;

import java_cup.runtime.Symbol;

class AtributosBloque {

    public String cad = "";
    public String cad2 = "";
    public String cad3= "";
    public String tipo = "";
    public int val = 0;
    public int val2 = 0;
    public String etq2 = "";
    public boolean banderajump= false;
    String etiq;
    public String valorg = "";
    public String claveg = "";

    public AtributosBloque(String etiqueta) {
        this.etiq = etiqueta;
    }
    
	public AtributosBloque() {
        
    }
	
	public void setEtiqueta(String e) {
        this.etiq = e;
    }

    public String getEtiqueta() {
        return etiq;
    }
}


%%


%{
 	public int getCantLineas(){
	  return yyline;
	}	
%}

%eofval{	
		return new Symbol(TokensSintacticoBloques.EOF);
%eofval}

%cup 
%line
%char
%class LexicoBloques
%public
%ignorecase


nument = "-"?[0-9]+("."[0-9]+)?
letra  = [a-zA-Z]
identificador = ({letra}|"$"|"_")({letra}|{nument}|"_"|"$")*
comentario = "//"([^\n])*

%%
"\"%c\""		   { return new Symbol(TokensSintacticoBloques.numero,yyline, yycolumn+1,yytext()); }
"\"%f\""		   { return new Symbol(TokensSintacticoBloques.numero,yyline, yycolumn+1,yytext()); }
"\"%d\""           { return new Symbol(TokensSintacticoBloques.numero,yyline, yycolumn+1,yytext()); }
"if"               { return new Symbol(TokensSintacticoBloques.dif, yyline, yycolumn+1, yytext());}
"if_not"           { return new Symbol(TokensSintacticoBloques.difnot, yyline, yycolumn+1, yytext());}
"goto"             { return new Symbol(TokensSintacticoBloques.jump, yyline, yycolumn+1, yytext());}
"int"              { return new Symbol(TokensSintacticoBloques.dec, yyline, yycolumn+1, yytext());}
"method"           { return new Symbol(TokensSintacticoBloques.proc, yyline, yycolumn+1, yytext());}
"{"                { return new Symbol(TokensSintacticoBloques.begin, yyline, yycolumn+1, yytext());}
"}"                { return new Symbol(TokensSintacticoBloques.end, yyline, yycolumn+1, yytext());}
"call"             { return new Symbol(TokensSintacticoBloques.call, yyline, yycolumn+1, yytext());}
"print"            { return new Symbol(TokensSintacticoBloques.print, yyline, yycolumn+1, yytext());}
"fprint"           { return new Symbol(TokensSintacticoBloques.fprint, yyline, yycolumn+1, yytext());}
"float"           { return new Symbol(TokensSintacticoBloques.float_, yyline, yycolumn+1, yytext());}
"-"                { return new Symbol(TokensSintacticoBloques.menos, yyline, yycolumn+1, yytext()); }
"+"                { return new Symbol(TokensSintacticoBloques.mas, yyline, yycolumn+1,yytext()); }
"*"                { return new Symbol(TokensSintacticoBloques.por, yyline, yycolumn+1, yytext()); }
"/"                { return new Symbol(TokensSintacticoBloques.div, yyline, yycolumn+1, yytext()); }
"%"                { return new Symbol(TokensSintacticoBloques.mod, yyline, yycolumn+1, yytext()); }
"="                { return new Symbol(TokensSintacticoBloques.asig, yyline, yycolumn+1, yytext()); }
"<"                { return new Symbol(TokensSintacticoBloques.men, yyline, yycolumn+1, yytext()); }
">"                { return new Symbol(TokensSintacticoBloques.may, yyline, yycolumn+1, yytext()); }
"<="               { return new Symbol(TokensSintacticoBloques.meni, yyline, yycolumn+1, yytext()); }
">="               { return new Symbol(TokensSintacticoBloques.mayi, yyline, yycolumn+1, yytext()); }
"=="               { return new Symbol(TokensSintacticoBloques.igual, yyline, yycolumn+1, yytext()); }
"!="               { return new Symbol(TokensSintacticoBloques.dife, yyline, yycolumn+1, yytext()); }
"("                { return new Symbol(TokensSintacticoBloques.parena, yyline, yycolumn+1, yytext()); }
")"                { return new Symbol(TokensSintacticoBloques.parenc, yyline, yycolumn+1, yytext()); }
"["                { return new Symbol(TokensSintacticoBloques.corcha, yyline, yycolumn+1, yytext()); }
"]"                { return new Symbol(TokensSintacticoBloques.corchc, yyline, yycolumn+1, yytext()); }
":"                { return new Symbol(TokensSintacticoBloques.dospuntos, yyline, yycolumn+1, yytext());}
";"                { return new Symbol(TokensSintacticoBloques.pcoma, yyline, yycolumn+1, yytext()); }
"."                { return new Symbol(TokensSintacticoBloques.punto, yyline, yycolumn+1, yytext()); }
","                { return new Symbol(TokensSintacticoBloques.coma, yyline, yycolumn+1, yytext());}
{nument}           { return new Symbol(TokensSintacticoBloques.numero, yyline, yycolumn+1, yytext());}
{identificador}    { return new Symbol(TokensSintacticoBloques.id, yyline, yycolumn+1, yytext());}
{comentario}       { }
[ \t\r\f\n]+       { }
.                  {System.err.println("Error Lexico "+yytext()+" linea: "+String.valueOf(yyline+1)+" columna "+String.valueOf(yychar+1)); }


