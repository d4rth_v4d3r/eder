package org.compi2;

import java_cup.runtime.*;

%%
%{
	private Symbol sym(int type) {
		return sym(type, yytext());
	}

	private Symbol sym(int type, Object value) {
		return new Symbol(type, yyline + 1, yycolumn + 1, value);
	}
%}

%init{
		
%init}

%eofval{	
		return sym(TokensP.EOF);
%eofval}

%class LexicoP
%public
%cup
%unicode
%ignorecase
%line  	//definimos el uso de contador de lineas
%char  	//definimos el uso de contador de caracteres
%column
%state COMMENT1, COMMENT2

cadena		=	\"(\\\"|[^\"])*\"

%%

<YYINITIAL>
	{
	
		"proyecto"	{	return sym(TokensP.PROYECTO);	}
		"nombre"	{	return sym(TokensP.NOMBRE);	}
		"ruta"		{	return sym(TokensP.RUTA);	}
		"archivos"	{	return sym(TokensP.ARCHIVOS);	}
		"archivo"	{	return sym(TokensP.ARCHIVO);	}
		"principal"	{	return sym(TokensP.PRINCIPAL);	}
		"="			{	return sym(TokensP.IGUAL);	}
		"<"			{	return sym(TokensP.MAYOR);	}
		">"			{	return sym(TokensP.MENOR);	}
		"/"			{	return sym(TokensP.DIV);	}
		{cadena}	{	return sym(TokensP.VSTRING, yytext().substring(yytext().indexOf("\"") + 1, yytext().lastIndexOf("\"")));	}
	
	//=================================== INICIO ESPACIOS EN BLANCO, COMENTARIOS Y ERRORES  =======================================	
		[" "\t]+	{}
		[\n\r\f]+	{}		
		.			{}						
	}
