package org.compi2;

import java_cup.runtime.*;

%%
%{
	private Symbol sym(int type) {
		return sym(type, yytext());
	}

	private Symbol sym(int type, Object value) {
		return new Symbol(type, yyline + 1, yycolumn + 1, value);
	}

%}

%init{
		
%init}

%eofval{	
		return sym(TokensT.EOF);
%eofval}

%class LexicoT
%public
%cup
%unicode
%ignorecase
%line  	//definimos el uso de contador de lineas
%char  	//definimos el uso de contador de caracteres
%column
%state COMMENT1, COMMENT2

character	= 	[A-Za-z]
digit		=	[0-9]
cadena		=	\"(\\\"|[^\"])*\"
entero		=	{digit}+
decimal		= 	{entero}"."{entero}
id 			=	{character}({character}|{digit}|"_")*
caracter	=	"'"."'"
archivo		=	{id}".frc"

%%

////////////////////////////////////////////////////////////////////////////////////////7

<YYINITIAL>
	{
		"int"		{	return sym(TokensT.ENTERO);	}
		"float"		{	return sym(TokensT.DECIMAL);	}
		"boolean"	{	return sym(TokensT.LOGICO);	}
		"string"	{	return sym(TokensT.CADENA);	}
		"char"		{	return sym(TokensT.CARACTER);	}
		"="			{	return sym(TokensT.ASIGN);	}
		{entero}	{	return sym(TokensT.VALENTERO, new Integer(yytext()));	}
		{decimal}	{	return sym(TokensT.VALDECIMAL, new Float(yytext()));	}
		{cadena}	{	String cadena = 
							new String(yytext().replaceAll("\\\\n", "\n")
									.replaceAll("\\\\t", "\t")
										.replaceAll("\\\\\"", "\0")
											.replaceAll("\"", "")
												.replaceAll("\0", "\""));;
						return sym(TokensT.VALCADENA, cadena);	}
		{caracter}	{	return sym(TokensT.VALCARACTER, new Integer(yytext().charAt(1)));	}
		"true"		{	return sym(TokensT.VALLOGICO, "1");	}
		"false"		{	return sym(TokensT.VALLOGICO, "0");	}
		{archivo}	{	return sym(TokensT.ARCHIVO);	}
		"++"		{	return sym(TokensT.INCREMENTO);	}
		"--"		{	return sym(TokensT.DECREMENTO);	}
		"#"			{	return sym(TokensT.NUMERAL);	}
		"."			{	return sym(TokensT.PUNTO);	}
		","			{	return sym(TokensT.COMA);	}
		"+"			{	return sym(TokensT.MAS);	}
		"-"			{	return sym(TokensT.MENOS);	}
		"*"			{	return sym(TokensT.POR);	}
		"/"			{	return sym(TokensT.DIV);	}
		"^"			{	return sym(TokensT.EXP);	}
		"("			{	return sym(TokensT.PARI);	}
		")"			{	return sym(TokensT.PARD);	}
		"{"			{	return sym(TokensT.LLAVEI);	}
		"}"			{	return sym(TokensT.LLAVED);	}
		"["			{	return sym(TokensT.BRACKETI);	}
		"]"			{	return sym(TokensT.BRACKETD);	}
		"&&"		{	return sym(TokensT.AND);	}
		"||"		{	return sym(TokensT.OR);	}
		"!"			{	return sym(TokensT.NOT);	}
		"&"			{	return sym(TokensT.REF);	}
		"=="		{	return sym(TokensT.IGUAL);	}
		"!="		{	return sym(TokensT.NO_IGUAL);	}
		"<"			{	return sym(TokensT.MENOR);	}
		">"			{	return sym(TokensT.MAYOR);	}
		"<="		{	return sym(TokensT.MENOR_IGUAL);	}
		">="		{	return sym(TokensT.MAYOR_IGUAL);	}
		"?"			{	return sym(TokensT.TERNARIO);	}
		":"			{	return sym(TokensT.DPUNTOS);	}
		"+="		{	return sym(TokensT.MASIGUAL);	}
		"-="		{	return sym(TokensT.MENOSIGUAL);	}
		"*="		{	return sym(TokensT.PORIGUAL);	}
		"/="		{	return sym(TokensT.DIVISIONIGUAL);	}
		"while"		{	return sym(TokensT.MIENTRAS);	}
		"do"		{	return sym(TokensT.HACER);	}
		"for"		{	return sym(TokensT.PARA);	}
		"continue"	{	return sym(TokensT.SALTARCICLO);	}
		"break"		{	return sym(TokensT.SALTO);	}
		"if"		{	return sym(TokensT.SI);	}
		"else"		{	return sym(TokensT.SINO);	}
		"switch"	{	return sym(TokensT.SELECTOR);	}
		"case"		{	return sym(TokensT.ENCASO);	}
		"array"		{	return sym(TokensT.ARREGLO);	}
		"new"		{	return sym(TokensT.NUEVO);	}
		"public"	{	return sym(TokensT.PUBLICO);	}
		"private"	{	return sym(TokensT.PRIVADO);	}
		"void"		{	return sym(TokensT.VACIO);	}
		"return"	{	return sym(TokensT.RETORNAR);	}
		"class"		{	return sym(TokensT.CLASE);	}
		"extends"	{	return sym(TokensT.HEREDA);	}
		"this"		{	return sym(TokensT.THIS);	}
		"import"	{	return sym(TokensT.IMPORTAR);	}
		"default"	{	return sym(TokensT.PORDEFECTO);	}
		"imprimir"	{	return sym(TokensT.IMPRIMIR);	}
		{id}		{ 	return sym(TokensT.ID, yytext().toLowerCase());  }
	
	//=================================== INICIO ESPACIOS EN BLANCO, COMENTARIOS Y ERRORES  =======================================	
		[" "\t]+	{ }
		"//"		{ yybegin(COMMENT1); }
		"/*"		{ yybegin(COMMENT2); }	
		.			{ Errores.errorLexico(String.format("Token no reconocido '%s'.", yytext()), yyline + 1, yycolumn + 1); }						
	}

		[\n\r\f]+	{ if(yystate() == COMMENT1) yybegin(YYINITIAL);	}
<COMMENT2>	"*/"	{ yybegin(YYINITIAL); }
<COMMENT1, COMMENT2>	.	{ }
		
