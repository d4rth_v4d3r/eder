package org.compi2;

import java_cup.runtime.*;

%%
%{
	private Symbol sym(int type) {
		return sym(type, yytext());
	}

	private Symbol sym(int type, Object value) {
		return new Symbol(type, yyline + 1, yycolumn + 1, value);
	}

%}

%init{
		
%init}

%eofval{	
		return sym(Tokens.EOF);
%eofval}

%class Lexico
%public
%cup
%unicode
%ignorecase
%line  	//definimos el uso de contador de lineas
%char  	//definimos el uso de contador de caracteres
%column
%state COMMENT1, COMMENT2

character	= 	[A-Za-z]
digit		=	[0-9]
cadena		=	\"(\\\"|[^\"])*\"
entero		=	{digit}+
decimal		= 	{entero}"."{entero}
id 			=	{character}({character}|{digit}|"_")*
caracter	=	"'"."'"
archivo		=	{id}".frc"

%%

////////////////////////////////////////////////////////////////////////////////////////7

<YYINITIAL>
	{
		"int"		{	return sym(Tokens.ENTERO);	}
		"float"		{	return sym(Tokens.DECIMAL);	}
		"boolean"	{	return sym(Tokens.LOGICO);	}
		"string"	{	return sym(Tokens.CADENA);	}
		"char"		{	return sym(Tokens.CARACTER);	}
		"="			{	return sym(Tokens.ASIGN);	}
		{entero}	{	return sym(Tokens.VALENTERO, new Integer(yytext()));	}
		{decimal}	{	return sym(Tokens.VALDECIMAL, new Float(yytext()));	}
		{cadena}	{	String cadena = 
							new String(yytext().replaceAll("\\\\n", "\n")
									.replaceAll("\\\\t", "\t")
										.replaceAll("\\\\\"", "\0")
											.replaceAll("\"", "")
												.replaceAll("\0", "\""));;
						return sym(Tokens.VALCADENA, cadena);	}
		{caracter}	{	return sym(Tokens.VALCARACTER, new Integer(yytext().charAt(1)));	}
		"true"		{	return sym(Tokens.VALLOGICO, "1");	}
		"false"		{	return sym(Tokens.VALLOGICO, "0");	}
		{archivo}	{	return sym(Tokens.ARCHIVO);	}
		"++"		{	return sym(Tokens.INCREMENTO);	}
		"--"		{	return sym(Tokens.DECREMENTO);	}
		"#"			{	return sym(Tokens.NUMERAL);	}
		"."			{	return sym(Tokens.PUNTO);	}
		","			{	return sym(Tokens.COMA);	}
		"+"			{	return sym(Tokens.MAS);	}
		"-"			{	return sym(Tokens.MENOS);	}
		"*"			{	return sym(Tokens.POR);	}
		"/"			{	return sym(Tokens.DIV);	}
		"^"			{	return sym(Tokens.EXP);	}
		"("			{	return sym(Tokens.PARI);	}
		")"			{	return sym(Tokens.PARD);	}
		"{"			{	return sym(Tokens.LLAVEI);	}
		"}"			{	return sym(Tokens.LLAVED);	}
		"["			{	return sym(Tokens.BRACKETI);	}
		"]"			{	return sym(Tokens.BRACKETD);	}
		"&&"		{	return sym(Tokens.AND);	}
		"||"		{	return sym(Tokens.OR);	}
		"!"			{	return sym(Tokens.NOT);	}
		"&"			{	return sym(Tokens.REF);	}
		"=="		{	return sym(Tokens.IGUAL);	}
		"!="		{	return sym(Tokens.NO_IGUAL);	}
		"<"			{	return sym(Tokens.MENOR);	}
		">"			{	return sym(Tokens.MAYOR);	}
		"<="		{	return sym(Tokens.MENOR_IGUAL);	}
		">="		{	return sym(Tokens.MAYOR_IGUAL);	}
		"?"			{	return sym(Tokens.TERNARIO);	}
		":"			{	return sym(Tokens.DPUNTOS);	}
		"+="		{	return sym(Tokens.MASIGUAL);	}
		"-="		{	return sym(Tokens.MENOSIGUAL);	}
		"*="		{	return sym(Tokens.PORIGUAL);	}
		"/="		{	return sym(Tokens.DIVISIONIGUAL);	}
		"while"		{	return sym(Tokens.MIENTRAS);	}
		"do"		{	return sym(Tokens.HACER);	}
		"for"		{	return sym(Tokens.PARA);	}
		"continue"	{	return sym(Tokens.SALTARCICLO);	}
		"break"		{	return sym(Tokens.SALTO);	}
		"if"		{	return sym(Tokens.SI);	}
		"else"		{	return sym(Tokens.SINO);	}
		"switch"	{	return sym(Tokens.SELECTOR);	}
		"case"		{	return sym(Tokens.ENCASO);	}
		"array"		{	return sym(Tokens.ARREGLO);	}
		"new"		{	return sym(Tokens.NUEVO);	}
		"public"	{	return sym(Tokens.PUBLICO);	}
		"private"	{	return sym(Tokens.PRIVADO);	}
		"void"		{	return sym(Tokens.VACIO);	}
		"return"	{	return sym(Tokens.RETORNAR);	}
		"class"		{	return sym(Tokens.CLASE);	}
		"extends"	{	return sym(Tokens.HEREDA);	}
		"this"		{	return sym(Tokens.THIS);	}
		"import"	{	return sym(Tokens.IMPORTAR);	}
		"default"	{	return sym(Tokens.PORDEFECTO);	}
		"imprimir"	{	return sym(Tokens.IMPRIMIR);	}
		{id}		{ 	return sym(Tokens.ID, yytext().toLowerCase());  }
	
	//=================================== INICIO ESPACIOS EN BLANCO, COMENTARIOS Y ERRORES  =======================================	
		[" "\t]+	{ }
		"//"		{ yybegin(COMMENT1); }
		"/*"		{ yybegin(COMMENT2); }	
		.			{ Errores.errorLexico(String.format("Token no reconocido '%s'.", yytext()), yyline + 1, yycolumn + 1); }						
	}

		[\n\r\f]+	{ if(yystate() == COMMENT1) yybegin(YYINITIAL);	}
<COMMENT2>	"*/"	{ yybegin(YYINITIAL); }
<COMMENT1, COMMENT2>	.	{ }
		
